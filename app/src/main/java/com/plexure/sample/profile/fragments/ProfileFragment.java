package com.plexure.sample.profile.fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.plexure.sample.R;
import com.plexure.sample.main.activities.AuthenticationActivity;
import com.plexure.sample.main.constants.ActivityTracking;
import com.plexure.sample.utils.UIUtils;

import co.vmob.sdk.VMob;
import co.vmob.sdk.VMobException;

/**
 * @author Ludy Su (ludy.su@plexure.com)
 */
public class ProfileFragment extends Fragment {

	private Button mButtonLogout;
	private Button mButtonLogin;

	private boolean mIsLoggedIn;


	public static ProfileFragment newInstance() {
		return new ProfileFragment();
	}

	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		View view;

		mIsLoggedIn = VMob.getInstance().getAuthenticationManager().isLoggedIn();
		if (mIsLoggedIn) {
			view = inflater.inflate(R.layout.fragment_profile, container, false);
			mButtonLogout = (Button) view.findViewById(R.id.btn_logout);
		} else {
			view = inflater.inflate(R.layout.fragment_profile_not_loggedin, container, false);
			mButtonLogin = (Button) view.findViewById(R.id.btn_goto_login);
		}

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (mIsLoggedIn) {
			mButtonLogout.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
					builder.setTitle("Logout").setMessage("Do you want to logout?")
							.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									VMob.getInstance().getAuthenticationManager().logout(new VMob.ResultCallback<Void>() {

										@Override
										public void onSuccess(Void aVoid) {
											//Go to authentication screen after logged out
											startAuthenticationActivity();
											getActivity().finish();
										}

										@Override
										public void onFailure(VMobException e) {
											UIUtils.showErrorToast(getActivity(), R.string.profile_lougout_error, e);
										}
									});
								}
							})
							.setNegativeButton("Cancel", null);
				}
			});
		} else {
			mButtonLogin.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					//Go back to authentication screen
					startAuthenticationActivity();
				}
			});
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		//Send view impression Plexure Activity for the screen.
		//We specify two parameters only: the unique code for the page and the generic
		//category for the page type.
		VMob.getInstance().getActivityTracker().logPageImpression(
				ActivityTracking.PAGE_IMPRESSION_PROFILE,
				null, null, null,
				ActivityTracking.PAGE_IMPRESSION_CODE_CONTENT);
	}

	private void startAuthenticationActivity() {
		Intent intent = new Intent(getActivity(), AuthenticationActivity.class);
		intent.putExtra(AuthenticationActivity.EXTRA_FORCE_SHOW_AUTH_SCREEN, true);
		startActivity(intent);
	}

}

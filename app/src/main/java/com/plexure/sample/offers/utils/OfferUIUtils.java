package com.plexure.sample.offers.utils;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.plexure.sample.R;
import com.plexure.sample.utils.PlexureUtils;
import com.squareup.picasso.Picasso;

import co.vmob.sdk.common.model.ImageFormat;
import co.vmob.sdk.content.offer.model.Offer;
import co.vmob.sdk.content.venue.model.Venue;

/**
 * Utility methods for UI view which displays an offer
 *
 * @author Almos Rajnai (almos.rajnai@plexure.com)
 */
public class OfferUIUtils {

	public static void populateOfferView(Context context, View view, Offer offer, Integer imageWidth) {
		((TextView) view.findViewById(R.id.title)).setText(offer.getTitle());
		((TextView) view.findViewById(R.id.description)).setText(offer.getDescription());

		//Get offer state
		PlexureUtils.OfferState offerState = PlexureUtils.evaluateVoucherState(offer);

		//Usually the state of the offer is displayed by applying a visual state
		//on the list item, for sake of simplicity we use a textual description instead.
		((TextView) view.findViewById(R.id.state)).setText(getOfferStateText(offerState));

		//Try to get the closest venue name, address and distance and
		//list it on the offer if available
		Venue venue = offer.getClosestVenue();
		TextView venueTextView = (TextView) view.findViewById(R.id.venue);
		if (venue != null) {
			//We have the closest venue, get the data to the screen
			venueTextView.setVisibility(View.VISIBLE);

			String venueText = context.getString(R.string.offers_address_format, venue.getName(), venue.getAddress(), venue.getCity(), venue.getDistanceFromCurrentLocation() / 1000.0f);
			venueTextView.setText(venueText);
		} else {
			//No closest venue available (e.g. location of the device is unknown), hide UI element
			venueTextView.setVisibility(View.GONE);
		}

		//Try to download the image for the advertisement using Picasso library.
		//To avoid too much traffic the image file format is set to JPEG.
		//The requested image size can be calculated in many ways, we are taking a simple approach:
		//same as the screen width. Not specifying the image height means the aspect ratio will
		//be maintained at image scaling on the server side.
		ImageView imageView = (ImageView) view.findViewById(R.id.image);
		Picasso.get().load(offer.getImageUrl(imageWidth, null, ImageFormat.JPEG)).placeholder(R.drawable.image_placeholder).into(imageView);
	}

	private static int getOfferStateText(PlexureUtils.OfferState offerState) {
		int textId;
		switch (offerState) {
			case AVAILABLE:
				textId = R.string.offers_state_available;
				break;

			case NOT_AVAILABLE_NON_RESPAWNABLE:
				textId = R.string.offers_state_not_available_not_respawnable;
				break;

			case NOT_AVAILABLE_RESPAWNABLE:
				textId = R.string.offers_state_not_available_respawnable;
				break;

			case ALREADY_REDEEMED_NOT_RESPAWNABLE:
				textId = R.string.offers_state_already_redeemed_not_respawnable;
				break;

			case ALREADY_REDEEMED_RESPAWNABLE:
				textId = R.string.offers_state_already_redeemed_respawnable;
				break;

			default:
				throw new RuntimeException("Unknown offer state: " + offerState.name());
		}

		return textId;
	}
}

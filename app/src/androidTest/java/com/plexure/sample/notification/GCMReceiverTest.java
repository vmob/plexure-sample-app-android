package com.plexure.sample.notification;

import android.content.Intent;
import android.os.Bundle;
import android.test.InstrumentationTestCase;

/**
 * @author Ludy Su (ludy.su@plexure.com)
 */
public class GCMReceiverTest extends InstrumentationTestCase {

	/**
	 * Triggers a GCM message locally and tests the behavior of {@link GCMReceiver}
	 */
	public void testOnReceive() throws Exception {
		Bundle bundle = new Bundle();
		bundle.putString("mId", "123");
		bundle.putString("notificationTitle", "My Title");
		bundle.putString("notificationText", "Notification message");
		bundle.putString("offerId", "356");
		bundle.putString("ed", "{\"deeplink\": \"https://www.google.com\"}");

		GCMReceiver receiver = new GCMReceiver();
		receiver.onReceive(getInstrumentation().getTargetContext(), new Intent().putExtras(bundle));
		Thread.sleep(30000);
	}

}
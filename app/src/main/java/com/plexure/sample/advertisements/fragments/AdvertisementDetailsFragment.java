package com.plexure.sample.advertisements.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.plexure.sample.R;

import co.vmob.sdk.VMob;
import co.vmob.sdk.content.advertisement.model.Advertisement;

/**
 * Sub-page fragment for displaying details for an advertisement using in-app browser.
 *
 * @author Ludy Su (ludy.su@plexure.com)
 */
public class AdvertisementDetailsFragment extends Fragment {

	private static final String TAG = AdvertisementDetailsFragment.class.getName();
	private static final String ARG_ADVERTISEMENT = "advertisement";

	private Advertisement mAdvertisement;
	private WebView mWebView;


	public static AdvertisementDetailsFragment newInstance(Advertisement advertisement) {
		//Advertisement for the fragment is stored as an argument for the fragment.
		//Advertisement instance is parcelable, so it can be stored safely in a bundle.
		AdvertisementDetailsFragment fragment = new AdvertisementDetailsFragment();
		Bundle arguments = new Bundle();
		arguments.putParcelable(ARG_ADVERTISEMENT, advertisement);
		fragment.setArguments(arguments);

		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//Get the advertisement for the fragment from the arguments
		Bundle arguments = getArguments();
		mAdvertisement = (Advertisement) arguments.get(ARG_ADVERTISEMENT);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_advertisement_details, container, false);
		mWebView = (WebView) view.findViewById(R.id.webview);

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		//Need a new instance of WebViewClient, otherwise it will open new links in the web page with a browser app.
		mWebView.setWebViewClient(new WebViewClient());
		mWebView.loadUrl(mAdvertisement.getClickThroughUrl());
	}

	@Override
	public void onStart() {
		super.onStart();

		//Send advertisement impression Plexure Activity
		VMob.getInstance().getActivityTracker().logAdImpression(mAdvertisement.getId(), mAdvertisement.getChannel(),
				mAdvertisement.getPlacement());
	}
}

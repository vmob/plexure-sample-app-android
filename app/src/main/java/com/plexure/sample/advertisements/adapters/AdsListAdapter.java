package com.plexure.sample.advertisements.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.plexure.sample.R;
import com.plexure.sample.utils.UIUtils;
import com.squareup.picasso.Picasso;

import co.vmob.sdk.VMob;
import co.vmob.sdk.common.model.ImageFormat;
import co.vmob.sdk.content.advertisement.model.Advertisement;

/**
 * List adapter for displaying advertisements directly from the Plexure SDK's {@link Advertisement}
 * instances.
 * <p/>
 * This class is pretty simple, doesn't do anything else than mapping the content of the fields from
 * the data model to the list view items.
 *
 * @author Almos Rajnai (almos.rajnai@plexure.com)
 */
public class AdsListAdapter<T> extends ArrayAdapter<T> {

	private final LayoutInflater mInflater;
	private final int mImageWidth;

	public AdsListAdapter(Activity activity) {
		super(activity.getApplicationContext(), R.layout.list_item_advertisement);

		mImageWidth = UIUtils.getScreenSize(activity).x;
		mInflater = activity.getLayoutInflater();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.list_item_advertisement, parent, false);
		}

		//Populate various UI components on the list item
		Advertisement advertisement = (Advertisement) getItem(position);
		((TextView) convertView.findViewById(R.id.title)).setText(advertisement.getTitle());
		((TextView) convertView.findViewById(R.id.description)).setText(advertisement.getDescription());

		//Try to download the image for the advertisement using Picasso library.
		//To avoid too much traffic the image file format is set to JPEG.
		//The requested image size can be calculated in many ways, we are taking a simple approach:
		//same as the screen width. Not specifying the image height means the aspect ratio will
		//be maintained at image scaling on the server side.
		ImageView imageView = (ImageView) convertView.findViewById(R.id.image);
		Picasso.get().load(advertisement.getImageUrl(mImageWidth, null, ImageFormat.JPEG)).placeholder(R.drawable
				.image_placeholder).into(imageView);

		//Send advertisement impression Plexure Activity.
		//This Activity must be sent for ads which have been actually displayed (and only for those).
		//In a standard list view the method can be called when the view for the list item is
		//populated (getView method, like here).
		//This simple solution might not work for other screen layout designs or components, most
		//notably it is not working with RecyclerView. In this case the displayed list items must be
		//tracked separately and the method must be called when an advertisement was displayed for
		//the first time or displayed again after it disappeared from the screen.
		VMob.getInstance().getActivityTracker().logAdImpression(
				advertisement.getId(),
				advertisement.getChannel(),
				advertisement.getPlacement());

		return convertView;
	}
}

package com.plexure.sample.main.constants;

/**
 * Constants for Plexure SDK Activity tracking: names of various events and categories.
 * <p/>
 * This file contains a few examples for IDs which are sent by the app to the Plexure backend
 * as Plexure Activities.
 * Collected activities are vary project by project, in this file you can find examples for
 * the following types:
 * <ul>
 * <li>Page impressions (page IDs and categories)</li>
 * <li>Custom button click activities</li>
 * </ul>
 *
 * @author Almos Rajnai (almos.rajnai@plexure.com)
 */
public class ActivityTracking {

	/***** Page impressions *****/

	/**
	 * Navigation drawer page impression
	 */
	public static final String PAGE_IMPRESSION_NAVIGATION_DRAWER = "navigation_drawer";

	/**
	 * Advertisements screen page impression
	 */
	public static final String PAGE_IMPRESSION_ADVERTISEMENTS = "advertisements_screen";

	/**
	 * Offers screen page impression
	 */
	public static final String PAGE_IMPRESSION_OFFERS = "offers_screen";

	/**
	 * Offer details screen page impression
	 */
	public static final String PAGE_IMPRESSION_OFFER_DETAILS = "offers_details_screen";

	/**
	 * Venues screen page impression
	 */
	public static final String PAGE_IMPRESSION_VENUES = "venues_screen";

	/**
	 * Loyalty cards page impression
	 */
	public static final String PAGE_IMPRESSION_LOYALTY_CARDS = "loyalty_cards_screen";

	/**
	 * Loyalty card instances page impression
	 */
	public static final String PAGE_IMPRESSION_LOYALTY_CARD_INSTANCE = "loyalty_cards_instance_screen";

	/**
	 * Authentication page impression
	 */
	public static final String PAGE_IMPRESSION_AUTHENTICATION = "authentication_screen";

	/**
	 * Profile page impression
	 */
	public static final String PAGE_IMPRESSION_PROFILE = "profile_screen";

	/**
	 * Settings page impression
	 */
	public static final String PAGE_IMPRESSION_SETTINGS = "settings_screen";

	/***** Page impression categories *****/

	/**
	 * Inner page impression category for content screens
	 */
	public static final String PAGE_IMPRESSION_CODE_CONTENT = "content";

	/**
	 * Inner page impression category for menu-type UI elements
	 */
	public static final String PAGE_IMPRESSION_CODE_MENU = "menu";


	/***** Button clicks *****/

	/**
	 * ID for clicking on a venue at the map
	 */
	public static final String BUTTON_CLICK_VENUE = "venue";

	/**
	 * ID for clicking on getting terms and conditions button at offer details screen
	 */
	public static final String BUTTON_CLICK_GET_TERMS = "terms_and_conditions";


	/***** Button click categories *****/

	/**
	 * Button click category for any generic click activity
	 */
	public static final String BUTTON_CLICK_CODE_GENERIC = "generic";
}

package com.plexure.sample.offers;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.plexure.sample.R;
import com.plexure.sample.main.activities.BaseActivity;
import com.plexure.sample.notification.GCMMessage;
import com.plexure.sample.offers.fragments.OfferDetailsFragment;
import com.plexure.sample.utils.UIUtils;

import co.vmob.sdk.VMob;
import co.vmob.sdk.VMobException;
import co.vmob.sdk.content.offer.model.Offer;

/**
 * Activity to show offer details when the app is launched by the push notification.
 */
public class OfferDetailsActivity extends BaseActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_offer_details);

		Intent intent = getIntent();

		//If it is invoked from push message with an offer ID, then load the offer from backend
		int offerId = intent.getIntExtra(GCMMessage.EXTRA_OFFER_ID, -1);
		if (offerId > 0) {
			VMob.getInstance().getOffersManager().getOffer(offerId, null, null, new VMob.ResultCallback<Offer>() {

				@Override
				public void onSuccess(Offer offer) {
					getFragmentManager().beginTransaction()
							.replace(R.id.container, OfferDetailsFragment.newInstance(offer))
							.commit();
				}

				@Override
				public void onFailure(VMobException e) {
					//Something went wrong, let's show the error on the screen
					UIUtils.showErrorToast(OfferDetailsActivity.this, R.string.offers_error, e);
				}
			});
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		//Navigate back to the previous screen
		if (item.getItemId() == android.R.id.home) {
			onBackPressed();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

}

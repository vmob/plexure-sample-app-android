package com.plexure.sample.main.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.plexure.sample.BuildConfig;
import com.plexure.sample.R;
import com.plexure.sample.main.constants.ActivityTracking;
import com.plexure.sample.main.constants.NavigationListItem;

import co.vmob.sdk.VMob;
import co.vmob.sdk.debug.DiagnosticsActivity;

/**
 * Standard navigation drawer for sub-fragments, derived from the Android SDK example code.
 * <p/>
 * Note: Please ignore this Fragment class, it does not contain any important information about
 * the implementation of the integration with the Plexure SDK.
 * The only Plexure SDK-related code is: page impression sending when the navigation drawer is opened.
 *
 * @author Almos Rajnai (almos.rajnai@plexure.com)
 */
public class NavigationDrawerFragment extends Fragment {

	private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";

	private NavigationDrawerCallbacks mCallbacks;
	private ActionBarDrawerToggle mDrawerToggle;
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerListView;
	private View mFragmentContainerView;
	private int mCurrentSelectedPosition = 0;

	public NavigationDrawerFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (savedInstanceState != null) {
			mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mCallbacks = (NavigationDrawerCallbacks) getActivity();
		selectItem(mCurrentSelectedPosition);

		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		mDrawerListView = (ListView) inflater.inflate(
				R.layout.fragment_navigation_drawer, container, false);
		mDrawerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (BuildConfig.DEBUG && position == mDrawerListView.getAdapter().getCount() - 1) {
					// Restores current selected state of the list view
					mDrawerListView.setItemChecked(mCurrentSelectedPosition, true);
					mDrawerListView.setItemChecked(position, false);
					// Launches the diagnostics activity, which is the last item of the list view
					Intent intent = new Intent(getActivity(), DiagnosticsActivity.class);
					startActivity(intent);
				} else {
					selectItem(position);
				}
			}
		});

		mDrawerListView.setAdapter(new ArrayAdapter<>(
				getActivity(),
				android.R.layout.simple_list_item_activated_1,
				android.R.id.text1,
				NavigationListItem.getNames(getActivity())));
		mDrawerListView.setItemChecked(mCurrentSelectedPosition, true);
		return mDrawerListView;
	}

	public boolean isDrawerOpen() {
		return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
	}

	public void setUp(int fragmentId, DrawerLayout drawerLayout) {
		mFragmentContainerView = getActivity().findViewById(fragmentId);
		mDrawerLayout = drawerLayout;

		mDrawerToggle = new ActionBarDrawerToggle(
				getActivity(),
				mDrawerLayout,
				R.string.navigation_drawer_open,
				R.string.navigation_drawer_close
		) {
			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
				if (!isAdded()) {
					return;
				}

				getActivity().invalidateOptionsMenu();
			}

			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				if (!isAdded()) {
					return;
				}

				getActivity().invalidateOptionsMenu();

				//Send view impression Plexure Activity for the screen.
				//We specify two parameters only: the unique code for the page and the generic
				//category for the page type.
				VMob.getInstance().getActivityTracker().logPageImpression(
						ActivityTracking.PAGE_IMPRESSION_NAVIGATION_DRAWER,
						null, null, null,
						ActivityTracking.PAGE_IMPRESSION_CODE_MENU);
			}
		};

		mDrawerLayout.post(new Runnable() {
			@Override
			public void run() {
				mDrawerToggle.syncState();
			}
		});
		mDrawerLayout.setDrawerListener(mDrawerToggle);
	}

	private void selectItem(int position) {
		mCurrentSelectedPosition = position;
		if (mDrawerListView != null) {
			mDrawerListView.setItemChecked(position, true);
		}
		if (mDrawerLayout != null) {
			mDrawerLayout.closeDrawer(mFragmentContainerView);
		}
		if (mCallbacks != null) {
			mCallbacks.onNavigationDrawerItemSelected(position);
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mCallbacks = null;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		//Is the drawer functioning in normal mode?
		if (mDrawerToggle.isDrawerIndicatorEnabled()) {
			//Yes: just call standard event handlers
			return mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
		}

		//No: a sub-page fragment is on the screen, trigger back button behavior
		getActivity().onBackPressed();

		return true;
	}

	public void setSubPageModeEnabled(boolean enabled) {
		if (enabled) {
			//Set up back arrow instead of the menu button
			mDrawerToggle.setDrawerIndicatorEnabled(false);

			//Disable navigation drawer
			mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
		} else {
			//Restore menu button
			mDrawerToggle.setDrawerIndicatorEnabled(true);

			//Enable navigation drawer
			mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
		}
	}

	public interface NavigationDrawerCallbacks {
		void onNavigationDrawerItemSelected(int position);
	}
}

package com.plexure.sample.offers.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.plexure.sample.R;
import com.plexure.sample.offers.utils.OfferUIUtils;
import com.plexure.sample.utils.UIUtils;

import co.vmob.sdk.VMob;
import co.vmob.sdk.common.model.ExternalConstants;
import co.vmob.sdk.content.offer.model.Offer;

/**
 * List adapter for displaying offers directly from the Plexure SDK's {@link Offer}
 * instances.
 * <p/>
 * This class is pretty simple, doesn't do anything else than mapping the content of the fields from
 * the data model to the list view items.
 *
 * @author Almos Rajnai (almos.rajnai@plexure.com)
 */
public class OffersListAdapter<T> extends ArrayAdapter<T> {

	private final LayoutInflater mInflater;
	private final Context mContext;
	private final int mImageWidth;

	public OffersListAdapter(Activity activity) {
		super(activity.getApplicationContext(), R.layout.list_item_offer);

		mContext = activity.getApplicationContext();
		mInflater = activity.getLayoutInflater();
		mImageWidth = UIUtils.getScreenSize(activity).x;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.list_item_offer, parent, false);
		}

		//Populate various UI components on the list item
		Offer offer = (Offer) getItem(position);
		OfferUIUtils.populateOfferView(mContext, convertView, offer, mImageWidth);

		//Send offer impression Plexure Activity.
		//We assume that all items are standard offers, but refer to the ExternalConstants class
		//for more possibilities.
		//This Activity must be sent for offers which have been actually displayed (and only for those).
		//In a standard list view the method can be called when the view for the list item is
		//populated (getView method, like here).
		//This simple solution might not work for other screen layout designs or components, most
		//notably it is not working with RecyclerView. In this case the displayed list items must be
		//tracked separately and the method must be called when an offer was displayed for the first
		//time or displayed again after it disappeared from the screen.
		VMob.getInstance().getActivityTracker().logOfferImpression(
				offer.getId(),
				ExternalConstants.OFFER_TYPE_NORMAL,
				ExternalConstants.OFFER_PLACEMENT_NORMAL);

		return convertView;
	}
}

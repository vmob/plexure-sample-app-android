## ProGuard rules for Plexure SDK Sample app
## Author: Almos Rajnai <almos.rajnai@plexure.com>
##
## Note: this rule set is extending the default 'proguard-android-optimize.txt' set from Android SDK
##

#
# Rules for Crashlythics
#
# Keeping source line numbers for the output, but renaming the source file name strings
#
-renamesourcefileattribute SourceFile
-keepattributes SourceFile,LineNumberTable

#
# Rules for okhttp library used by Picasso
#
-dontwarn okio.**
-dontwarn okhttp3.**

# ---- START Needed for Plexure SDK -----

# AltBeacon needs these rules to detect beacons after app being killed
-dontwarn org.altbeacon.**
-keep class org.altbeacon.** { *; }
-keep interface org.altbeacon.** { *; }

# Keep annotations for JSON deserialization
-keepattributes *Annotation*
-keepattributes Signature
-keep class * {
    @com.google.gson.annotations.* <fields>;
}

-dontwarn com.google.android.gms.**
-keep class com.google.android.gms.** { *; }
-keepattributes EnclosingMethod

# ---- END Needed for Plexure SDK -----
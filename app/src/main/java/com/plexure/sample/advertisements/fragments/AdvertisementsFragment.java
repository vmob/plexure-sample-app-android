package com.plexure.sample.advertisements.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;

import com.plexure.sample.R;
import com.plexure.sample.advertisements.adapters.AdsListAdapter;
import com.plexure.sample.main.activities.MainActivity;
import com.plexure.sample.main.constants.ActivityTracking;
import com.plexure.sample.main.fragments.ListFragment;
import com.plexure.sample.utils.PlexureUtils;

import java.util.List;

import co.vmob.sdk.VMob;
import co.vmob.sdk.VMobException;
import co.vmob.sdk.content.BaseContent;
import co.vmob.sdk.content.advertisement.model.AdSearchCriteria;
import co.vmob.sdk.content.advertisement.model.Advertisement;

/**
 * Fragment for fetching and displaying advertisements from the Plexure backend.
 * <p/>
 * This fragment displays the list of advertisements which are fetched from the Plexure backend.
 * Together with the list adapter it implements a simple way for handling the process for getting
 * the advertisements and manage failure scenarios.
 *
 * @author Almos Rajnai (almos.rajnai@plexure.com)
 */
public class AdvertisementsFragment extends ListFragment<BaseContent> {

	private static final String TAG = AdvertisementsFragment.class.getName();


	@Override
	protected ArrayAdapter<BaseContent> createListAdapter(Activity activity) {
		return new AdsListAdapter<>(activity);
	}

	@Override
	protected void showItemDetailsScreen(BaseContent item) {
		//Handling item click: open URL from advertisement as a generic URL.
		//It is up to the implementation project how to handle the click through URLs from
		//an advertisements.
		Advertisement advert = (Advertisement) item;
		AdvertisementDetailsFragment fragment = AdvertisementDetailsFragment.newInstance(advert);

		MainActivity mainActivity = (MainActivity) getActivity();
		mainActivity.openChildFragment(R.string.ad_details_screen_title, fragment);

		//Send advertisement click Plexure Activity
		VMob.getInstance().getActivityTracker().logAdClick(advert.getId(), advert.getChannel(), advert.getPlacement());
	}

	protected void loadItems() {
		//We don't set up any filtering for the advertisements, but this object would
		//specify any required filtering
		AdSearchCriteria criteria = new AdSearchCriteria.Builder().create();

		//Let's call the SDK's method which will do the request for us
		//Result callback is inlined for simplifying the code
		VMob.getInstance().getAdvertisementsManager().getAdvertisements(criteria, new VMob.ResultCallback<List<Advertisement>>() {

			@Override
			public void onSuccess(List<Advertisement> advertisements) {
				//Fresh list of advertisements is available, set it on the adapter
				if (advertisements.size() > 0) {
					mAdapter.addAll(advertisements);
				} else {
					//We don't have any ads in the list, let the user know about
					setEmptyListHeader(R.string.ads_no_items);
				}
			}

			@Override
			public void onFailure(VMobException e) {
				//Something went wrong, let's show the error on the screen
				setEmptyListHeader(R.string.ads_error, PlexureUtils.getErrorText(e));
			}
		});
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mLodingTextRes = R.string.ads_loading;
		mPageImpressionName = ActivityTracking.PAGE_IMPRESSION_ADVERTISEMENTS;
	}

}

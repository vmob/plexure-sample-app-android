package com.plexure.sample.loyaltycard.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.plexure.sample.R;
import com.plexure.sample.utils.UIUtils;
import com.squareup.picasso.Picasso;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import co.vmob.sdk.VMob;
import co.vmob.sdk.VMobException;
import co.vmob.sdk.common.model.ImageFormat;
import co.vmob.sdk.content.loyaltycard.model.LoyaltyCard;
import co.vmob.sdk.content.loyaltycard.model.LoyaltyCardInstance;
import co.vmob.sdk.content.loyaltycard.model.LoyaltyCardPoints;

/**
 * List adapter for displaying {@link LoyaltyCardInstance}s.
 *
 * @author Ludy Su (ludy.su@plexure.com)
 */
public class StampCardInstancesListAdapter<T> extends ArrayAdapter<T> {

	private static final String TAG = StampCardInstancesListAdapter.class.getName();

	private static final int STAMPS_TO_ADD = 1;
	private static final boolean AUTO_ACTIVATE_REWARD = true;
	private static final boolean FILL_MULTIPLE_CARDS = true;

	private LayoutInflater mInflater;
	private int mImageWidth;
	private LoyaltyCard mLoyaltyCard;
	private Activity mActivity;

	public StampCardInstancesListAdapter(Activity activity, LoyaltyCard card) {
		super(activity.getApplicationContext(), R.layout.list_item_stamp_card_instance);

		mImageWidth = UIUtils.getScreenSize(activity).x;
		mInflater = activity.getLayoutInflater();
		mLoyaltyCard = card;
		mActivity = activity;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.list_item_stamp_card_instance, parent, false);

			//Store component views for future use, no need to findViewById() every time
			ViewHolder holder = new ViewHolder();
			holder.mImageView = (ImageView) convertView.findViewById(R.id.image);
			holder.mStampBar = (RatingBar) convertView.findViewById(R.id.stamp_bar);
			holder.mAddStampButton = (Button) convertView.findViewById(R.id.button_add_stamp);
			convertView.setTag(holder);
		}

		final ViewHolder holder = (ViewHolder) convertView.getTag();
		final LoyaltyCardInstance card = (LoyaltyCardInstance) getItem(position);
		holder.mStampBar.setNumStars(mLoyaltyCard.getPointsRequired());
		holder.mStampBar.setRating(card.getPointsBalance());
		// Reached max stamps, so disable the add stamp button
		if (card.getPointsBalance() >= mLoyaltyCard.getPointsRequired()) {
			holder.mAddStampButton.setEnabled(false);
		} else {
			holder.mAddStampButton.setEnabled(true);
		}

		holder.mAddStampButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
				builder.setTitle(R.string.stamp_card_instance_add_stamp_dialog_title);
				builder.setMessage(R.string.stamp_card_instance_add_stamp_dialog_message);

				builder.setPositiveButton(R.string.stamp_card_instance_add_stamp_dialog_add, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						addStamp(holder, card);
					}
				});

				builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});

				builder.create().show();
			}
		});

		//Try to download the image for the stamp card using Picasso library.
		//To avoid too much traffic the image file format is set to JPEG.
		//The requested image size can be calculated in many ways, we are taking a simple approach:
		//same as the screen width. Not specifying the image height means the aspect ratio will
		//be maintained at image scaling on the server side.
		Picasso.get().load(mLoyaltyCard.getCardImageUrl(mImageWidth, null, ImageFormat.JPEG))
				.placeholder(R.drawable.image_placeholder)
				.into(holder.mImageView);

		return convertView;
	}

	private void addStamp(final ViewHolder holder, final LoyaltyCardInstance instance) {
		// Using UUID.randomUUID().toString() is not a common practice for generating a transaction ID, you should have
		// your own implementation of that unique ID. This is only used here cause it's a generic app.
		VMob.getInstance().getLoyaltyCardsManager().addPoints(instance.getLoyaltyCardId(), STAMPS_TO_ADD,
				UUID.randomUUID().toString(), AUTO_ACTIVATE_REWARD, FILL_MULTIPLE_CARDS,
				new VMob.ResultCallback<LoyaltyCardPoints>() {

					@Override
					public void onSuccess(LoyaltyCardPoints loyaltyCardPoints) {
						// After adding points to the stamp card instance, it takes some time (from a seconds to
						// a few minutes) to get the {@link LoyaltyCardInstance} with new points balance.
						holder.mStampBar.setRating(loyaltyCardPoints.getPointsBalance());
						// Reached max stamps, so disable the add stamp button
						if (loyaltyCardPoints.getPointsBalance() >= mLoyaltyCard.getPointsRequired()) {
							holder.mAddStampButton.setEnabled(false);
						}
						updateStampCardInstancesFromServer();
					}

					@Override
					public void onFailure(VMobException e) {
						UIUtils.showErrorToast(mActivity, R.string.stamp_card_instance_add_stamp_error, e);
					}
				});
	}

	// TODO: updating from server and refresh the whole UI screen are not best practices, will address them in ticket ANDROID-882.
	private void updateStampCardInstancesFromServer() {
		clear();

		// load stamp card instances from server
		VMob.getInstance().getLoyaltyCardsManager().getLoyaltyCards(new VMob.ResultCallback<List<LoyaltyCard>>() {

			@Override
			public void onSuccess(List<LoyaltyCard> loyaltyCards) {
				for (LoyaltyCard card : loyaltyCards) {
					if (card.getId() == mLoyaltyCard.getId()) {
						mLoyaltyCard = card;
						addAll((Collection<? extends T>) sortLoyaltyCardInstances(card.getInstances()));
						break;
					}
				}
			}

			@Override
			public void onFailure(VMobException e) {
				UIUtils.showErrorToast(mActivity, R.string.stamp_card_instance_error, e);
			}
		});
	}

	/**
	 * Moves the active stamp card instance to the top of the list.
	 */
	public static List<LoyaltyCardInstance> sortLoyaltyCardInstances(List<LoyaltyCardInstance> loyaltyCardInstances) {
		LinkedList<LoyaltyCardInstance> result = new LinkedList<>();
		for (LoyaltyCardInstance instance : loyaltyCardInstances) {
			if (instance.isActive()) {
				result.addFirst(instance);
			} else {
				result.add(instance);
			}
		}
		return result;
	}

	/*
 	 * Inner classes, interfaces, enums
 	 */

	/**
	 * Improve list view loading performance by storing component views, so that we don't need to call
	 * {@link View#findViewById(int)} repeatedly.
	 *
	 * @see <a href="http://developer.android.com/training/improving-layouts/smooth-scrolling.html#ViewHolder">Hold View Objects in a View Holder</a>
	 */
	private static class ViewHolder {
		public ImageView mImageView;
		public RatingBar mStampBar;
		public Button mAddStampButton;
	}

}

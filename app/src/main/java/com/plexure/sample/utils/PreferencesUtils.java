package com.plexure.sample.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Date;

/**
 * Utility methods for handling data saved to the shared preferences
 *
 * @author Almos Rajnai (almos.rajnai@plexure.com)
 */
public class PreferencesUtils {

	private static final String PREFERENCES_NAME = "Preferences";

	public static final String KEY_LOGIN_SKIPPED = "login_skipped";

	private static final String KEY_VENUES_TIMESTAMP = "venues_timestamp";

	private static SharedPreferences sSharedPreferences;


	public static void init(Context context) {
		sSharedPreferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
	}

	public static void saveBoolean(final String key, final boolean value) {
		sSharedPreferences.edit().putBoolean(key, value).apply();
	}

	public static boolean getBoolean(final String key) {
		return sSharedPreferences.getBoolean(key, false);
	}

	public static void saveString(int key, final String value) {
		saveString(String.valueOf(key), value);
	}

	public static void saveString(final String key, final String value) {
		sSharedPreferences.edit().putString(key, value).apply();
	}

	public static String getString(int key) {
		return getString(String.valueOf(key));
	}

	public static String getString(final String key) {
		return sSharedPreferences.getString(key, null);
	}

	/**
	 * Save new venue update timestamp with current time (UTC) to preferences.
	 */
	public static void saveNewVenueUpdateTimestamp() {
		Date now = new Date(DateUtils.getCurrentUtcTimeInMilliseconds());

		//Save timestamp as a long representation of the current time
		PreferencesUtils.saveString(PreferencesUtils.KEY_VENUES_TIMESTAMP, String.valueOf(now.getTime()));
	}

	/**
	 * Get venue update timestamp from preferences (UTC).
	 *
	 * @return Timestamp from preferences or null if not available.
	 */
	public static Date getVenueUpdateTimestamp() {
		//Try to get the timestamp from the prefs
		Date timestamp = null;
		try {
			String timestampString = PreferencesUtils.getString(PreferencesUtils.KEY_VENUES_TIMESTAMP);

			//Try to parse it as long data
			timestamp = new Date(Long.parseLong(timestampString));
		} catch (NumberFormatException e) {
			//Something went wrong: we don't have the timestamp
		}

		return timestamp;
	}
}

package com.plexure.sample.main;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.plexure.sample.utils.PreferencesUtils;
import com.plexure.sample.venues.providers.VenueProvider;

import co.vmob.sdk.VMob;
import co.vmob.sdk.VMobException;
import io.fabric.sdk.android.Fabric;

/**
 * Android Application class for the app.
 *
 * @author Almos Rajnai (almos.rajnai@plexure.com)
 */
public class SampleApplication extends Application {

    private static final String TAG = SampleApplication.class.getName();


    @Override
    public void onCreate() {
        super.onCreate();

        Fabric.with(this, new Crashlytics());

        // Initialize preferences handling utility class
        PreferencesUtils.init(this);

        // Initialize Plexure SDK and show a Toast notification about the result
        VMob.init(this, mInitResultHandler);

        // Initialize provider for venue data handling
        VenueProvider.init(this);
    }

    /**
     * Result callback handler for Plexure SDK initialization.
     * <p/>
     * Please keep in mind that when you develop the application the SDK initialization might
     * fail due to various reasons. Your application must be able to handle the situation,
     * preferably provide a retry mechanism for the initialization.
     * If the SDK initialization had failed then most of the SDK functionality is not available.
     */
    private final VMob.ResultCallback<Void> mInitResultHandler = new VMob.ResultCallback<Void>() {
        @Override
        public void onSuccess(Void data) {
            showToast("Plexure SDK initialization finished");
        }

        @Override
        public void onFailure(VMobException exception) {
            Log.e(TAG, "Plexure SDK initialization failed", exception);

            showToast(String.format("Plexure SDK initialization failed: %s : %s", exception.getClass().getName(), exception.getMessage()));
        }
    };

    private void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

}

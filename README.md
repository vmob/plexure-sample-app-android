# Sample app for Plexure Android SDK

## Introduction
This source code implements a basic Android app which makes use of various features of the Plexure SDK.

IMPORTANT: Plexure (formerly VMob Ltd.) was re-branded in 2016. In this app, the occurrences of the previous name (i.e., VMob)
were replaced by Plexure accordingly, where possible. In the SDK source code, the re-branding is scheduled for version 5. Thus,
the old name (i.e., VMob) is still being used in the current SDK version (4.x.x).

## Setup
The source code is based on standard Gradle build, can be built directly using the included Gradle
wrapper or imported into Android Studio.

Before you begin using the source code you need to add a file which is not part of the source
code, but required for running the sample app:

### Plexure SDK configuration
To be able to use the Plexure SDK, you first need to set up the configuration file for it, which is an
Android resource file. Configuration values should be added to a file in the directory (*app/src/main/res/values/vmob_config.xml*).

The possible configuration items are documented in the Getting Started section in the
[Plexure SDK documentation]. There are three required items, which must be present:

* *vmob_site_id* - Site ID for the Plexure backend
* *vmob_auth_key* - Authentication key for the Plexure backend

Other fields might also be required for certain features, for example push notification sender ID: *vmob_gcm_sender_id*

An example for the configuration files structure:

```
<?xml version="1.0" encoding="utf-8"?>
<resources xmlns:tools="http://schemas.android.com/tools" tools:ignore="TypographyDashes">

    <string name="vmob_site_id"> ...Plexure backend site ID... </string>
    <string name="vmob_auth_key"> ...Plexure backend authentication key... </string>
    <string name="vmob_gcm_sender_id"> ...GCM sender ID... </string>

</resources>
```

## How to use the sources
After successful configuration the application can be built using standard Gradle build process.
The app binary result can be installed and ran on any device which has Android OS 4.0.3 (API level 15) or higher
and Google Play Services installed.

Since this source code implements a functioning application it contains more lines than what would
be absolutely necessary to make use of the Plexure SDK.

Please review the comments in the source files thoroughly, important details are described about
the implementation details for the Plexure SDK.

## See also
For further information please refer to the [Plexure SDK documentation].

[Plexure SDK documentation]:http://mobile.plexure.com/getting_started/introduction.html
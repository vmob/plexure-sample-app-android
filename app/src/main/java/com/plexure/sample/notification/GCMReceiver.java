package com.plexure.sample.notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.plexure.sample.utils.UIUtils;

/**
 * Simple {@link BroadcastReceiver} for handling GCM messages.
 *
 * @author Almos Rajnai (almos.rajnai@plexure.com)
 */
public class GCMReceiver extends BroadcastReceiver {

	private static final String TAG = GCMReceiver.class.getName();

	public static final String EXTRA_NOTIFICATION_WAS_CLICKED = "notification_was_clicked";
	public static final String EXTRA_NOTIFICATION_ID = "notification_id";


	@Override
	public void onReceive(Context context, Intent intent) {
		//Extract data from received message
		GCMMessage gcmMessage = new GCMMessage(intent.getExtras());

		//If there was no message and no mTitle in the notification then we skip it
		if (gcmMessage.getMessage() != null || gcmMessage.getTitle() != null) {
			UIUtils.showNotification(context, gcmMessage);
		}

		Log.d(TAG, "GCM message received: " + gcmMessage.getTitle());
	}

}

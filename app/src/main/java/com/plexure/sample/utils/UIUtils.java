package com.plexure.sample.utils;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.NotificationCompat;
import android.view.Display;
import android.view.WindowManager;
import android.widget.Toast;

import com.plexure.sample.R;
import com.plexure.sample.main.activities.MainActivity;
import com.plexure.sample.notification.GCMMessage;
import com.plexure.sample.notification.GCMReceiver;
import com.plexure.sample.offers.OfferDetailsActivity;

import co.vmob.sdk.VMobException;

/**
 * Various user interface-related utility methods
 * <p/>
 * Note: Please ignore this utility class, it does not contain any important information about
 * the implementation of the integration with the Plexure SDK.
 *
 * @author Almos Rajnai (almos.rajnai@plexure.com)
 */
public class UIUtils {

	private static int sNotificationId = 0;


	public static Point getScreenSize(Context context) {
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);

		return size;
	}

	/**
	 * Shows a notification message in the system status bar area.
	 */
	public static void showNotification(Context context, @NonNull GCMMessage gcmMessage) {
		Intent intent = new Intent();

		//Add an extra as indication to the launched Activity that it was started by a notification click
		//If EXTRA_NOTIFICATION_WAS_CLICKED is true, then BaseActivity will log this push message click
		intent.putExtra(GCMReceiver.EXTRA_NOTIFICATION_WAS_CLICKED, true);
		intent.putExtra(GCMReceiver.EXTRA_NOTIFICATION_ID, gcmMessage.getMessageId());

		if (gcmMessage.getOfferId() > 0) {
			intent.setClass(context, OfferDetailsActivity.class);
			intent.setAction(Intent.ACTION_MAIN);
			intent.addCategory(Intent.CATEGORY_LAUNCHER);

			//Goto the offer page if an offer id exists
			intent.putExtra(GCMMessage.EXTRA_OFFER_ID, gcmMessage.getOfferId());
		} else if (gcmMessage.getDeepLink() != null) {
			//Goto the deep link URL if exists
			intent.setAction(Intent.ACTION_VIEW);
			intent.setData(Uri.parse(gcmMessage.getDeepLink()));
		} else {
			intent.setClass(context, MainActivity.class);
			intent.setAction(Intent.ACTION_MAIN);
			intent.addCategory(Intent.CATEGORY_LAUNCHER);
		}

		//To be able to send the Plexure Activity for notification click we need to start
		//the main activity from scratch, otherwise it won't be possible to detect
		//the new intent.
		//This approach might not be suitable to all implementations, in that case
		//it is important to find a way for passing the message ID to the launched
		//Android Activity and send the message clicked Plexure Activity by calling the
		//appropriate Plexure SDK method.
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

		PendingIntent contentIntent = PendingIntent.getActivity(context, 0, intent, 0);

		NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
				.setContentIntent(contentIntent)
				.setContentTitle(gcmMessage.getTitle())
				.setContentText(gcmMessage.getMessage())
				.setSmallIcon(R.drawable.ic_notification)
				.setDefaults(Notification.DEFAULT_ALL)
				.setAutoCancel(true);

		//For Lollipop we need to set both the content text and the "big text",
		//otherwise the notification might appear as empty when collapsed.
		//Text for these two fields might be different, depends on the implementation.
		builder.setStyle(new NotificationCompat.BigTextStyle().bigText(gcmMessage.getMessage()));

		//New fields introduced from Lollipop
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			builder.setCategory(Notification.CATEGORY_PROMO).setVisibility(Notification.VISIBILITY_PUBLIC);
		}

		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(++sNotificationId, builder.build());
	}

	public static void showAlertDialog(Context context, String title, String message) {
		new AlertDialog.Builder(context)
				.setTitle(title)
				.setMessage(message)
				.setPositiveButton(android.R.string.ok, null)
				.create()
				.show();
	}

	public static void showErrorToast(Context context, @StringRes int messageResId, VMobException exception) {
		Toast.makeText(context, context.getString(messageResId, PlexureUtils.getErrorText(exception)), Toast.LENGTH_SHORT).show();
	}

}

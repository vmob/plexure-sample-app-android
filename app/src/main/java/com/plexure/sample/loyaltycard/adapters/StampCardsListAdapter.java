package com.plexure.sample.loyaltycard.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.plexure.sample.R;
import com.plexure.sample.utils.UIUtils;
import com.squareup.picasso.Picasso;

import co.vmob.sdk.VMob;
import co.vmob.sdk.common.model.ImageFormat;
import co.vmob.sdk.content.loyaltycard.model.LoyaltyCard;

/**
 * List adapter for displaying {@link LoyaltyCard}s.
 *
 * @author Ludy Su (ludy.su@plexure.com)
 */
public class StampCardsListAdapter<T> extends ArrayAdapter<T> {

	private final LayoutInflater mInflater;
	private Activity mActivity;
	private final int mImageWidth;

	public StampCardsListAdapter(Activity activity) {
		super(activity.getApplicationContext(), R.layout.list_item_stamp_card);

		mImageWidth = UIUtils.getScreenSize(activity).x;
		mActivity = activity;
		mInflater = activity.getLayoutInflater();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.list_item_stamp_card, parent, false);

			//Store component views for future use, no need to findViewById() every time
			ViewHolder holder = new ViewHolder();
			holder.mImageView = (ImageView) convertView.findViewById(R.id.image);
			holder.mTitle = (TextView) convertView.findViewById(R.id.title);
			holder.mDescription = (TextView) convertView.findViewById(R.id.description);
			holder.mMaxInstances = (TextView) convertView.findViewById(R.id.max_instances);
			holder.mPointsForOffer = (TextView) convertView.findViewById(R.id.points_for_offer);
			holder.mOffersAvailable = (TextView) convertView.findViewById(R.id.offers_available);
			convertView.setTag(holder);
		}

		ViewHolder holder = (ViewHolder) convertView.getTag();
		LoyaltyCard card = (LoyaltyCard) getItem(position);
		holder.mTitle.setText(card.getTitle());
		holder.mDescription.setText(card.getDescription());
		holder.mMaxInstances.setText(String.valueOf(card.getMaxInstances()));
		holder.mPointsForOffer.setText(String.valueOf(card.getPointsRequired()));
		holder.mOffersAvailable.setText(String.valueOf(card.getOffers().size()));

		//Try to download the image for the stamp card using Picasso library.
		//To avoid too much traffic the image file format is set to JPEG.
		//The requested image size can be calculated in many ways, we are taking a simple approach:
		//same as the screen width. Not specifying the image height means the aspect ratio will
		//be maintained at image scaling on the server side.
		Picasso.get().load(card.getCardImageUrl(mImageWidth, null, ImageFormat.JPEG))
				.placeholder(R.drawable.image_placeholder)
				.into(holder.mImageView);

		//Send stamp card impression Plexure Activity.
		//This Activity must be sent for loyalty cards which have been actually displayed (and only for those).
		//In a standard list view the method can be called when the view for the list item is
		//populated (getView method, like here).
		//This simple solution might not work for other screen layout designs or components, most
		//notably it is not working with RecyclerView. In this case the displayed list items must be
		//tracked separately and the method must be called when an advertisement was displayed for
		//the first time or displayed again after it disappeared from the screen.
		VMob.getInstance().getActivityTracker().logLoyaltyCardImpression(card.getId());

		return convertView;
	}

	/*
     * Inner classes, interfaces, enums
     */

	/**
	 * Improve list view loading performance by storing component views, so that we don't need to call
	 * {@link View#findViewById(int)} repeatedly.
	 *
	 * @see <a href="http://developer.android.com/training/improving-layouts/smooth-scrolling.html#ViewHolder">Hold View Objects in a View Holder</a>
	 */
	private static class ViewHolder {
		ImageView mImageView;
		TextView mTitle;
		TextView mDescription;
		TextView mMaxInstances;
		TextView mPointsForOffer;
		TextView mOffersAvailable;
	}

}

package com.plexure.sample.venues.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.plexure.sample.R;

import java.util.Locale;

import co.vmob.sdk.content.venue.model.Venue;

/**
 * Utility methods for location-related operations
 *
 * @author Almos Rajnai (almos.rajnai@plexure.com)
 */
public class LocationUtils {

	private static final String TAG = LocationUtils.class.getName();

	/**
	 * Open Google Maps app (or other navigation app) and get directions to a venue from the current
	 * location of the user (device).
	 * If the current location is not available then user will be notified using a {@link Toast}.
	 *
	 * @param context App {@link Context}
	 * @param venue   Destination venue
	 */
	public static void getDirectionsToVenue(Context context, Venue venue) {
		//Try to find the user's current location and use it for getting directions for navigation.
		Location userLocation = getCurrentLocation(context);

		if (userLocation != null) {
			//Create Google Maps URL for getting directions:
			//user location is the starting point, selected venue is destination
			//Please note: using the US locale is important for formatting numbers in the URL
			final String url = String.format(Locale.US, "http://maps.google.com/maps?saddr=%f,%f&daddr=%f,%f",
					userLocation.getLatitude(), userLocation.getLongitude(),
					venue.getLatitude(), venue.getLongitude());

			//Open URL as a standard intent action
			try {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
				context.startActivity(intent);
			} catch (Exception e) {
				Log.e(TAG, "Opening URL for navigation failed, URL: " + url, e);
			}
		} else {
			//Current location is not available, inform user
			Toast.makeText(context,
					context.getResources().getString(R.string.venues_location_not_available),
					Toast.LENGTH_SHORT)
					.show();
		}
	}

	/**
	 * Get current location of the user (device).
	 *
	 * @param context App {@link Context}
	 * @return Location served by {@link LocationManager), or null if current location is not available.
	 */
	public static Location getCurrentLocation(Context context) {
		if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
				&& ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			// TODO: Consider calling ActivityCompat#requestPermissions here to request the missing permissions, and then overriding
			//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
			//                                          int[] grantResults)
			// to handle the case where the user grants the permission. See the documentation for ActivityCompat#requestPermissions
			// for more details.

			return null;
		}

		LocationManager service = (LocationManager) context.getSystemService(Activity.LOCATION_SERVICE);
		String provider = service.getBestProvider(new Criteria(), false);

		return service.getLastKnownLocation(provider);
	}

	/**
	 * Calculate the distance from a start location to a venue location.
	 *
	 * @param venue	Destination venue
	 * @param startLoc Start location
	 * @return distance in metres
	 */
	public static float getDistanceFromVenue(Location startLoc, Venue venue) {
		Location loc = new Location("");
		loc.setLatitude(venue.getLatitude());
		loc.setLongitude(venue.getLongitude());

		return startLoc.distanceTo(loc);
	}
}

package com.plexure.sample.notification;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Simple GCM Message model.
 *
 * @author Ludy Su (ludy.su@plexure.com)
 */
public class GCMMessage {

	private static final String TAG = GCMMessage.class.getName();

	public static final String EXTRA_OFFER_ID = "offerId";

	private static final String EXTRA_MESSAGE_ID = "mId";
	private static final String EXTRA_MESSAGE_TITLE = "notificationTitle";
	private static final String EXTRA_MESSAGE_TEXT = "notificationText";
	private static final String EXTRA_EXTENDED_DATA = "ed";

	private static final Pattern DEEP_LINK_PATTERN = Pattern.compile(".*(https?://[^\\s\"]*).*");

	//Message ID is set to -1 by default
	private int mMessageId = -1;
	private int mOfferId = -1;
	private String mTitle;
	private String mMessage;
	private String mDeepLink;


	/*package*/ GCMMessage(Bundle bundle) {
		//Extract message data from the bundle sent by the receiver
		if (bundle != null) {
			String messageIdString = bundle.getString(EXTRA_MESSAGE_ID);
			String offerIdString = bundle.getString(EXTRA_OFFER_ID);
			//Try to parse the message ID into an integer
			try {
				mMessageId = Integer.parseInt(messageIdString);
				mOfferId = Integer.parseInt(offerIdString);
			} catch (NumberFormatException e) {
				//Not a number, we will skip it and keep the ID set to -1
				Log.w(TAG, "Failed to parse the GCM message ID or offer ID to numbers", e);
			}

			mTitle = bundle.getString(EXTRA_MESSAGE_TITLE);
			mMessage = bundle.getString(EXTRA_MESSAGE_TEXT);

			String extendedData = bundle.getString(EXTRA_EXTENDED_DATA);
			if (!TextUtils.isEmpty(extendedData)) {
				Matcher matcher = DEEP_LINK_PATTERN.matcher(extendedData);
				if (matcher.matches()) {
					mDeepLink = matcher.group(1);
					Log.d(TAG, "Found deep link: " + mDeepLink);
				} else {
					Log.v(TAG, "No deep link found in the push message");
				}
			}
		}
	}

	public int getMessageId() {
		return mMessageId;
	}

	public String getTitle() {
		return mTitle;
	}

	public String getMessage() {
		return mMessage;
	}

	public int getOfferId() {
		return mOfferId;
	}

	public String getDeepLink() {
		return mDeepLink;
	}
}

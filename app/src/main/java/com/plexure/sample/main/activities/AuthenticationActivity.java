package com.plexure.sample.main.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.plexure.sample.R;
import com.plexure.sample.main.fragments.AuthenticationDialogFragment;
import com.plexure.sample.utils.PreferencesUtils;
import com.plexure.sample.utils.UIUtils;

import co.vmob.sdk.VMob;
import co.vmob.sdk.VMobException;


/**
 * @author Ludy Su (ludy.su@plexure.com)
 */
public class AuthenticationActivity extends BaseActivity implements VMob.ResultCallback<Void> {

	public static final String EXTRA_FORCE_SHOW_AUTH_SCREEN = "force_auth_screen";

	private int mDialogType;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (!getIntent().getBooleanExtra(EXTRA_FORCE_SHOW_AUTH_SCREEN, false)
				//If user chose to skip login before, just go to main activity
				&& (PreferencesUtils.getBoolean(PreferencesUtils.KEY_LOGIN_SKIPPED)
				|| VMob.getInstance().getAuthenticationManager().isLoggedIn())) {
			startMainActivity();
			return;
		}

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_authentication);

		findViewById(R.id.btn_login).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mDialogType = AuthenticationDialogFragment.DIALOG_TYPE_LOGIN;
				startDialog();
			}
		});

		findViewById(R.id.btn_signup).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mDialogType = AuthenticationDialogFragment.DIALOG_TYPE_SIGNUP;
				startDialog();
			}
		});

		findViewById(R.id.tv_forgot).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mDialogType = AuthenticationDialogFragment.DIALOG_TYPE_FORGOT_PASSWORD;
				startDialog();
			}
		});

		findViewById(R.id.tv_skip).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				//Go to main activity
				PreferencesUtils.saveBoolean(PreferencesUtils.KEY_LOGIN_SKIPPED, true);
				startMainActivity();
			}
		});
	}

	// Callback of AuthenticationDialogFragment
	@Override
	public void onSuccess(Void aVoid) {
		startMainActivity();
	}

	// Callback of AuthenticationDialogFragment
	@Override
	public void onFailure(VMobException e) {
		int titleResId = 0;
		switch (mDialogType) {
			case AuthenticationDialogFragment.DIALOG_TYPE_LOGIN:
				titleResId = R.string.auth_login_failed_msg;
				break;
			case AuthenticationDialogFragment.DIALOG_TYPE_SIGNUP:
				titleResId = R.string.auth_signup_failed_msg;
				break;
			case AuthenticationDialogFragment.DIALOG_TYPE_FORGOT_PASSWORD:
				titleResId = R.string.auth_forgot_failed_msg;
				break;
		}
		UIUtils.showErrorToast(this, titleResId, e);
	}

	private void startMainActivity() {
		Intent intent = new Intent(this, MainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		finish();
	}

	private void startDialog() {
		AuthenticationDialogFragment.newInstance(mDialogType).show(getFragmentManager(), AuthenticationActivity.class.getSimpleName());
	}

}

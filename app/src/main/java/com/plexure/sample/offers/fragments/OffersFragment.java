package com.plexure.sample.offers.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;

import com.plexure.sample.R;
import com.plexure.sample.main.activities.MainActivity;
import com.plexure.sample.main.constants.ActivityTracking;
import com.plexure.sample.main.fragments.ListFragment;
import com.plexure.sample.offers.adapters.OffersListAdapter;
import com.plexure.sample.utils.PlexureUtils;

import java.util.List;

import co.vmob.sdk.VMob;
import co.vmob.sdk.VMobException;
import co.vmob.sdk.common.model.ExternalConstants;
import co.vmob.sdk.content.BaseContent;
import co.vmob.sdk.content.offer.model.Offer;
import co.vmob.sdk.content.offer.model.OfferSearchCriteria;

/**
 * Fragment for fetching and displaying offers from Plexure backend.
 * <p/>
 * This fragment displays the list of offers which are fetched from the Plexure backend.
 * Together with the list adapter it implements a simple way for handling the process for getting
 * the offers and manage failure scenarios.
 *
 * @author Almos Rajnai (almos.rajnai@plexure.com)
 */
public class OffersFragment extends ListFragment<BaseContent> {

	@Override
	protected ArrayAdapter<BaseContent> createListAdapter(Activity activity) {
		return new OffersListAdapter<>(activity);
	}

	@Override
	protected void showItemDetailsScreen(BaseContent item) {
		//Handling item click: open details screen for the offer
		Offer offer = (Offer) item;
		OfferDetailsFragment fragment = OfferDetailsFragment.newInstance(offer);

		MainActivity mainActivity = (MainActivity) getActivity();
		mainActivity.openChildFragment(R.string.offer_details_screen_title, fragment);

		//Send offer click Plexure Activity.
		//We assume that all items are standard offers, but refer to the ExternalConstants class
		//for more possibilities.
		VMob.getInstance().getActivityTracker().logOfferClick(offer.getId(), ExternalConstants.OFFER_TYPE_NORMAL,
				ExternalConstants.OFFER_PLACEMENT_NORMAL);
	}

	protected void loadItems() {
		//We don't set up any filtering for the offers, but this object would
		//specify any required filtering
		OfferSearchCriteria criteria = new OfferSearchCriteria.Builder().create();

		//Let's call the SDK's method which will do the request for us
		//Result callback is inlined for simplifying the code
		VMob.getInstance().getOffersManager().getOffers(criteria, new VMob.ResultCallback<List<Offer>>() {

			@Override
			public void onSuccess(List<Offer> offers) {
				//Fresh list of offers is available, set it on the adapter
				if (offers.size() > 0) {
					mAdapter.addAll(offers);
				} else {
					//We don't have any offers in the list, let the user know about
					setEmptyListHeader(R.string.offers_no_items);
				}
			}

			@Override
			public void onFailure(VMobException e) {
				//Something went wrong, let's show the error on the screen
				setEmptyListHeader(R.string.offers_error, PlexureUtils.getErrorText(e));
			}
		});
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mLodingTextRes = R.string.offers_loading;
		mPageImpressionName = ActivityTracking.PAGE_IMPRESSION_OFFERS;
	}

}

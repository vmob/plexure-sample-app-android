package com.plexure.sample.main.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.plexure.sample.R;
import com.plexure.sample.main.constants.ActivityTracking;

import co.vmob.sdk.VMob;
import co.vmob.sdk.VMobException;
import co.vmob.sdk.consumer.model.LoginInfo;
import co.vmob.sdk.consumer.model.LoginResponse;
import co.vmob.sdk.consumer.model.SignUpInfo;
import co.vmob.sdk.consumer.model.SignUpType;

/**
 * @author Ludy Su (ludy.su@plexure.com)
 */
public class AuthenticationDialogFragment extends DialogFragment {

	private static final String ARG_DIALOG_TYPE = "is_sign_up";

	public static final int DIALOG_TYPE_LOGIN = 1;
	public static final int DIALOG_TYPE_SIGNUP = 2;
	public static final int DIALOG_TYPE_FORGOT_PASSWORD = 3;

	private int mDialogType;

	private VMob.ResultCallback<Void> mCallback;


	public static AuthenticationDialogFragment newInstance(int type) {
		if (type != DIALOG_TYPE_LOGIN && type != DIALOG_TYPE_SIGNUP && type != DIALOG_TYPE_FORGOT_PASSWORD) {
			throw new IllegalArgumentException("Invalid dialog type: " + type);
		}

		AuthenticationDialogFragment fragment = new AuthenticationDialogFragment();
		Bundle arguments = new Bundle();
		arguments.putInt(ARG_DIALOG_TYPE, type);
		fragment.setArguments(arguments);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle arguments = getArguments();
		mDialogType = arguments.getInt(ARG_DIALOG_TYPE);
	}

	@Override
	public void onStart() {
		super.onStart();

		//Send view impression Plexure Activity for the screen.
		//We specify two parameters only: the unique code for the page and the generic
		//category for the page type.
		VMob.getInstance().getActivityTracker().logPageImpression(ActivityTracking.PAGE_IMPRESSION_AUTHENTICATION,
				null, null, null, ActivityTracking.PAGE_IMPRESSION_CODE_CONTENT);
	}

	@Override
	public void onAttach(Context activity) {
		super.onAttach(activity);
		// Verify that the host activity implements the callback interface
		try {
			mCallback = (VMob.ResultCallback<Void>) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement ResultCallback");
		}
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		View dialogView = getActivity().getLayoutInflater().inflate(R.layout.dialog_authentication, null);
		builder.setView(dialogView);

		final EditText email = (EditText) dialogView.findViewById(R.id.et_email);
		final EditText password = (EditText) dialogView.findViewById(R.id.et_password);

		int titleResId = 0;
		switch (mDialogType) {
			case DIALOG_TYPE_LOGIN:
				titleResId = R.string.auth_login;
				break;
			case DIALOG_TYPE_SIGNUP:
				titleResId = R.string.auth_signup;
				break;
			case DIALOG_TYPE_FORGOT_PASSWORD:
				titleResId = R.string.auth_forgot;
				password.setEnabled(false);
				password.setVisibility(View.GONE);
				break;
		}
		builder.setTitle(titleResId);

		builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				//Do nothing
			}
		});

		builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				String emailString = email.getText().toString();
				String passwordString = password.getText().toString();

				if (TextUtils.isEmpty(emailString) || (TextUtils.isEmpty(passwordString) && mDialogType != DIALOG_TYPE_FORGOT_PASSWORD)) {
					Toast.makeText(getActivity(), R.string.auth_signup_fields_required, Toast.LENGTH_SHORT).show();
					return;
				}

				if (mDialogType == DIALOG_TYPE_SIGNUP) {
					final SignUpInfo.Builder infoBuilder = new SignUpInfo.Builder(SignUpType.EMAIL);
					infoBuilder.setEmailAddress(emailString);
					infoBuilder.setPassword(passwordString);

					VMob.getInstance().getAuthenticationManager().signUp(infoBuilder.create(), new VMob.ResultCallback<Void>() {

						@Override
						public void onSuccess(Void aVoid) {
							mCallback.onSuccess(null);
						}

						@Override
						public void onFailure(VMobException e) {
							mCallback.onFailure(e);
						}
					});
				} else if (mDialogType == DIALOG_TYPE_LOGIN) {
					LoginInfo.Builder infoBuilder = new LoginInfo.Builder();
					infoBuilder.setUsername(emailString);
					infoBuilder.setPassword(passwordString);

					VMob.getInstance().getAuthenticationManager().login(infoBuilder.create(), new VMob.ResultCallback<LoginResponse>() {

						@Override
						public void onSuccess(LoginResponse loginResponse) {
							mCallback.onSuccess(null);
						}

						@Override
						public void onFailure(VMobException e) {
							mCallback.onFailure(e);
						}
					});
				} else {
					VMob.getInstance().getAuthenticationManager().requestPasswordReset(emailString, new VMob.ResultCallback<Void>() {

						@Override
						public void onSuccess(Void aVoid) {
							mCallback.onSuccess(null);
						}

						@Override
						public void onFailure(VMobException e) {
							mCallback.onFailure(e);
						}
					});
				}
			}
		});

		return builder.create();
	}

}

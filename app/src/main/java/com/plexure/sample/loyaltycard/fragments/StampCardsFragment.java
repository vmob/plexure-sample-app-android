package com.plexure.sample.loyaltycard.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.plexure.sample.R;
import com.plexure.sample.loyaltycard.adapters.StampCardsListAdapter;
import com.plexure.sample.main.activities.MainActivity;
import com.plexure.sample.main.constants.ActivityTracking;
import com.plexure.sample.main.fragments.ListFragment;
import com.plexure.sample.utils.PlexureUtils;
import com.plexure.sample.utils.UIUtils;

import java.util.List;

import co.vmob.sdk.VMob;
import co.vmob.sdk.VMobException;
import co.vmob.sdk.content.loyaltycard.model.LoyaltyCard;
import co.vmob.sdk.content.loyaltycard.model.LoyaltyCardInstance;

/**
 * Fragment for fetching and displaying stamp cards from Plexure backend.
 * <p>
 * This fragment displays the list of stamp cards which are fetched from the Plexure backend. If you click on one of the
 * stamp cards, you can activate an instance of that stamp card and see all the instances you have.
 *
 * @author Ludy Su (ludy.su@plexure.com)
 */
public class StampCardsFragment extends ListFragment<LoyaltyCard> {

	private static final String TAG = StampCardsFragment.class.getName();


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mLodingTextRes = R.string.stamp_card_loading;
		mPageImpressionName = ActivityTracking.PAGE_IMPRESSION_LOYALTY_CARDS;
	}

	@Override
	protected ArrayAdapter<LoyaltyCard> createListAdapter(Activity activity) {
		return new StampCardsListAdapter<>(activity);
	}

	@Override
	protected void showItemDetailsScreen(final LoyaltyCard item) {
		//Don't activate a stamp card if the user is not logged in
		if (!VMob.getInstance().getAuthenticationManager().isLoggedIn()) {
			Toast.makeText(getActivity(), R.string.stamp_card_not_logged_in, Toast.LENGTH_SHORT).show();
			return;
		}

		List<LoyaltyCardInstance> instances = item.getInstances();

		if (instances.size() <= 0) {
			// We don't have any instance of this stamp card, try to activate one
			VMob.getInstance().getLoyaltyCardsManager().activateInstance(item.getId(), new VMob.ResultCallback<Void>() {

				@Override
				public void onSuccess(Void aVoid) {
					launchDetailFragment(item);
				}

				@Override
				public void onFailure(VMobException e) {
					UIUtils.showErrorToast(getActivity(), R.string.stamp_card_activate_instance_error, e);
				}
			});
		} else {
			launchDetailFragment(item);
		}
	}

	private void launchDetailFragment(LoyaltyCard card) {
		((MainActivity) getActivity()).openChildFragment(R.string.stamp_card_instance_screen_title,
				StampCardInstancesFragment.newInstance(card));
	}

	@Override
	protected void loadItems() {
		// load stamp cards from server
		VMob.getInstance().getLoyaltyCardsManager().getLoyaltyCards(new VMob.ResultCallback<List<LoyaltyCard>>() {

			@Override
			public void onSuccess(List<LoyaltyCard> loyaltyCards) {
				mAdapter.clear();
				if (loyaltyCards.size() > 0) {
					mAdapter.addAll(loyaltyCards);
				} else {
					//We don't have any stamp cards in the list, let the user know about
					setEmptyListHeader(R.string.stamp_card_no_items);
				}
			}

			@Override
			public void onFailure(VMobException e) {
				//Something went wrong, let's show the error on the screen
				setEmptyListHeader(R.string.stamp_card_error, PlexureUtils.getErrorText(e));
			}
		});
	}

}

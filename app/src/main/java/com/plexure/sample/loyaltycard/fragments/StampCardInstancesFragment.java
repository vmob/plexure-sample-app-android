package com.plexure.sample.loyaltycard.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;

import com.plexure.sample.R;
import com.plexure.sample.loyaltycard.adapters.StampCardInstancesListAdapter;
import com.plexure.sample.main.constants.ActivityTracking;
import com.plexure.sample.main.fragments.ListFragment;
import com.plexure.sample.utils.PlexureUtils;

import java.util.List;

import co.vmob.sdk.VMob;
import co.vmob.sdk.VMobException;
import co.vmob.sdk.content.loyaltycard.model.LoyaltyCard;
import co.vmob.sdk.content.loyaltycard.model.LoyaltyCardInstance;

/**
 * Fragment for fetching and displaying stamp card instances from Plexure backend.
 * <p>
 * This fragment displays the list of stamp card instances which are fetched from the Plexure backend.
 *
 * @author Ludy Su (ludy.su@plexure.com)
 */
public class StampCardInstancesFragment extends ListFragment<LoyaltyCardInstance> {

	private static final String TAG = StampCardInstancesFragment.class.getName();
	private static final String ARG_LOYALTY_CARD = "stamp_card_instance";

	private LoyaltyCard mLoyaltyCard;


	public static StampCardInstancesFragment newInstance(LoyaltyCard loyaltyCard) {
		//LoyaltyCard for the fragment is stored as an argument for the fragment.
		//LoyaltyCard parcelable, so it can be stored safely in a bundle.
		StampCardInstancesFragment fragment = new StampCardInstancesFragment();
		Bundle arguments = new Bundle();
		arguments.putParcelable(ARG_LOYALTY_CARD, loyaltyCard);
		fragment.setArguments(arguments);

		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//Get the LoyaltyCard for the fragment from the arguments
		Bundle arguments = getArguments();
		mLoyaltyCard = (LoyaltyCard) arguments.get(ARG_LOYALTY_CARD);

		mLodingTextRes = R.string.stamp_card_instance_loading;
		mPageImpressionName = ActivityTracking.PAGE_IMPRESSION_LOYALTY_CARD_INSTANCE;
	}

	@Override
	protected ArrayAdapter<LoyaltyCardInstance> createListAdapter(Activity activity) {
		return new StampCardInstancesListAdapter<>(activity, mLoyaltyCard);
	}

	@Override
	protected void showItemDetailsScreen(LoyaltyCardInstance item) {
		// No items details, do nothing
	}

	@Override
	protected void loadItems() {
		// There are instances inside the LoyaltyCard, we can use them directly
		if (mLoyaltyCard.getInstances().size() > 0) {
			mAdapter.clear();
			mAdapter.addAll(StampCardInstancesListAdapter.sortLoyaltyCardInstances(mLoyaltyCard.getInstances()));
			return;
		}

		// Load stamp card instances from server
		VMob.getInstance().getLoyaltyCardsManager().getLoyaltyCards(new VMob.ResultCallback<List<LoyaltyCard>>() {

			@Override
			public void onSuccess(List<LoyaltyCard> loyaltyCards) {
				mAdapter.clear();
				boolean found = false;
				for (LoyaltyCard card : loyaltyCards) {
					if (card.getId() == mLoyaltyCard.getId()) {
						mAdapter.addAll(StampCardInstancesListAdapter.sortLoyaltyCardInstances(card.getInstances()));
						found = true;
						break;
					}
				}

				if (!found) {
					//We don't have any stamp card instance in the list, let the user know about
					setEmptyListHeader(R.string.stamp_card_instance_no_items);
				}
			}

			@Override
			public void onFailure(VMobException e) {
				//Something went wrong, let's show the error on the screen
				setEmptyListHeader(R.string.stamp_card_instance_error, PlexureUtils.getErrorText(e));
			}
		});
	}

}

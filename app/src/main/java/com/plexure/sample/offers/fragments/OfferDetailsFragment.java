package com.plexure.sample.offers.fragments;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.plexure.sample.R;
import com.plexure.sample.main.constants.ActivityTracking;
import com.plexure.sample.offers.utils.OfferUIUtils;
import com.plexure.sample.utils.DateUtils;
import com.plexure.sample.utils.PlexureUtils;
import com.plexure.sample.utils.UIUtils;

import java.util.List;

import co.vmob.sdk.VMob;
import co.vmob.sdk.VMobException;
import co.vmob.sdk.content.offer.model.Offer;
import co.vmob.sdk.content.offer.model.RedeemedOffer;
import co.vmob.sdk.content.offer.model.RedeemedOfferSearchCriteria;
import co.vmob.sdk.network.error.ServerApiException;
import co.vmob.sdk.network.error.ServerError;

/**
 * Sub-page fragment for displaying details for an offer
 * <p/>
 * This screen has multiple purpose:
 * <ol>
 * <li>Shows all important details of the selected offer</li>
 * <li>Fetches the Terms & Conditions for the offer on a button click</li>
 * <li>Handles the redemption of the offer</li>
 * </ol>
 *
 * @author Almos Rajnai (almos.rajnai@plexure.com)
 */
public class OfferDetailsFragment extends Fragment {

	private static final String TAG = OfferDetailsFragment.class.getName();

	private static final String ARG_OFFER = "offer";

	private Offer mOffer;

	private Button mTermsButton;
	private View mTermsTitle;
	private TextView mTermsText;
	private Button mRedeemButton;
	private View mRedemptionCodeContainer;
	private TextView mRedemptionCode;
	private TextView mStateText;


	public static OfferDetailsFragment newInstance(Offer offer) {
		//Offer for the fragment is stored as an argument for the fragment.
		//Offer instance is parcelable, so it can be stored safely in a bundle.
		OfferDetailsFragment fragment = new OfferDetailsFragment();
		Bundle arguments = new Bundle();
		arguments.putParcelable(ARG_OFFER, offer);
		fragment.setArguments(arguments);

		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//Get the offer for the fragment from the arguments
		Bundle arguments = getArguments();
		mOffer = (Offer) arguments.get(ARG_OFFER);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_offer_details, container, false);

		//Set up fields with references to more UI elements
		mTermsTitle = view.findViewById(R.id.terms_title);
		mTermsText = (TextView) view.findViewById(R.id.terms_text);
		mRedemptionCodeContainer = view.findViewById(R.id.redemption_code_container);
		mRedemptionCode = (TextView) view.findViewById(R.id.redemption_code);
		mStateText = (TextView) view.findViewById(R.id.state);
		mTermsButton = (Button) view.findViewById(R.id.get_terms);
		mRedeemButton = (Button) view.findViewById(R.id.redeem);

		return view;
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		Context appContext = getActivity().getApplicationContext();
		View view = getView();
		int imageWidth = UIUtils.getScreenSize(appContext).x;

		//Populate UI elements from the offer model
		OfferUIUtils.populateOfferView(appContext, view, mOffer, imageWidth);

		// Add start and end date for the offer. All Date objects returned by Plexure SDK has not taken time zone into
		// account, so we have to add the time zone offset to it.
		((TextView) view.findViewById(R.id.start_date)).setText(getString(R.string.offer_details_start_date,
				DateUtils.addTimeZoneOffset((mOffer.getStartDate()))));
		((TextView) view.findViewById(R.id.end_date)).setText(getString(R.string.offer_details_end_date,
				DateUtils.addTimeZoneOffset((mOffer.getEndDate()))));

		//Get offer state and set up UI accordingly
		PlexureUtils.OfferState offerState = PlexureUtils.evaluateVoucherState(mOffer);

		//Set up buttons
		mTermsButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				getTerms();

				//Send Plexure Activity for clicking on get terms and conditions button.
				//We assume a generic click in this case, but specific information can be served as
				//context for the click via parameters which are not specified here.
				//The button click ID and the offer ID together gives enough information about the event.
				VMob.getInstance().getActivityTracker().logButtonClick(
						ActivityTracking.BUTTON_CLICK_GET_TERMS,
						null,
						null,
						mOffer.getId(),
						ActivityTracking.BUTTON_CLICK_CODE_GENERIC);
			}
		});

		//If the user is not logged in, don't allow him/her to redeem an offer
		if (!VMob.getInstance().getAuthenticationManager().isLoggedIn()) {
			mRedeemButton.setText(R.string.offer_details_redeem_button_login_required);
			mRedeemButton.setEnabled(false);

		//Was this offer redeemed previously and not available currently?
		} else if (offerState == PlexureUtils.OfferState.ALREADY_REDEEMED_NOT_RESPAWNABLE ||
				offerState == PlexureUtils.OfferState.ALREADY_REDEEMED_RESPAWNABLE) {

			//Offer was redeemed previously and not available currently:
			//hide redeem button and get the redemption code from the platform again
			mRedeemButton.setVisibility(View.GONE);
			getRedemptionCode();
		} else {
			mRedeemButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					redeemOffer();
				}
			});

			//Redeem button is enabled only if the offer can be redeemed (is active),
			//see offer state evaluation: PlexureUtils.evaluateVoucherState()
			mRedeemButton.setEnabled(offerState == PlexureUtils.OfferState.AVAILABLE);
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		//Send view impression Plexure Activity for the screen.
		//We specify two parameters only: the unique code for the page and the generic
		//category for the page type.
		VMob.getInstance().getActivityTracker().logPageImpression(
				ActivityTracking.PAGE_IMPRESSION_OFFER_DETAILS,
				null, null, null,
				ActivityTracking.PAGE_IMPRESSION_CODE_CONTENT);
	}

	//Get the terms and conditions text from the platform.
	//Please note: the text can be rather long, it is better to get it when it was necessary.
	private void getTerms() {
		Log.d(TAG, "Get terms & conditions for offer: " + mOffer.getId());
		final ProgressDialog progressDialog = ProgressDialog.show(getActivity(), null, getString(R.string.offer_details_get_terms_loading));

		VMob.getInstance().getOffersManager().getTermsAndConditions(mOffer.getId(), new VMob.ResultCallback<String>() {
			@Override
			public void onSuccess(String terms) {
				Log.d(TAG, "Terms and conditions for offer: " + mOffer.getId());
				progressDialog.dismiss();

				//Show T&C on the screen and hide button for getting it
				mTermsButton.setVisibility(View.GONE);
				mTermsTitle.setVisibility(View.VISIBLE);
				mTermsText.setVisibility(View.VISIBLE);
				mTermsText.setText(terms);
			}

			@Override
			public void onFailure(VMobException e) {
				Log.e(TAG, "Error while getting terms and conditions for offer: " + mOffer.getId(), e);
				progressDialog.dismiss();

				//Getting the terms failed, show error
				UIUtils.showAlertDialog(
						getActivity(),
						getString(R.string.title_error),
						getString(R.string.offer_details_get_terms_error, PlexureUtils.getErrorText(e)));
			}
		});
	}

	//Redeem the offer and handle the result
	private void redeemOffer() {
		Log.d(TAG, "Redeeming offer: " + mOffer.getId());

		final ProgressDialog progressDialog = ProgressDialog.show(getActivity(), null, getString(R.string.offer_details_redeem_loading));

		//Try to redeem the offer (without gift ID and offer instance ID)
		VMob.getInstance().getOffersManager().redeemOffer(mOffer.getId(), null, null, new VMob.ResultCallback<RedeemedOffer>() {
			@Override
			public void onSuccess(RedeemedOffer redeemedOffer) {
				Log.d(TAG, "Redeeming was successful for offer: " + mOffer.getId());
				progressDialog.dismiss();

				//Change displayed offer state, previous state is not valid anymore, but cannot be
				//evaluated yet without getting the offer again from the server.
				mStateText.setText(R.string.offers_state_already_redeemed_recently);

				//Offer was redeemed successfully, let's get the redemption code and show it on the screen
				changeToRedeemedState(redeemedOffer.getRedemptionText());
			}

			@Override
			public void onFailure(VMobException error) {
				Log.e(TAG, "Failed to redeem offer: " + mOffer.getId(), error);
				progressDialog.dismiss();

				//Default is a generic error message
				int errorId = R.string.offer_details_redeem_offer_error;

				//If the error is a server API exception and the type is CONFLICT then
				//this offer was redeemed already, we will change the message in the pop-up
				if (error instanceof ServerApiException &&
						((ServerApiException) error).getError() == ServerError.CONFLICT) {
					errorId = R.string.offer_details_already_redeemed_error;
				}

				//Show error pop-up with the selected string ID for the error message
				UIUtils.showAlertDialog(
						getActivity(),
						getString(R.string.title_error),
						getString(errorId, PlexureUtils.getErrorText(error)));
			}
		});
	}

	//When an offer was redeemed previously then we need the redemption code whenever the user
	//comes back to the offer. This method is trying to get the redemption data from the platform
	//and if it was available then show it.
	//Please note: it is possible that the redemption data was not available anymore, your app
	//must be able to handle that situation.
	private void getRedemptionCode() {
		Log.d(TAG, "Get redemption code for offer: " + mOffer.getId());

		final ProgressDialog progressDialog = ProgressDialog.show(getActivity(), null, getString(R.string.offer_details_get_redeemed_offer_loading));

		//No need to filter the redeemed offers, we are trying to get the non-expired redemption data only
		RedeemedOfferSearchCriteria criteria = new RedeemedOfferSearchCriteria.Builder().create();

		VMob.getInstance().getOffersManager().getRedeemedOffers(criteria, new VMob.ResultCallback<List<RedeemedOffer>>() {
			@Override
			public void onSuccess(List<RedeemedOffer> redeemedOffers) {
				progressDialog.dismiss();

				//We have received all redeemed offers, try to find the current offer
				for (RedeemedOffer redeemedOffer : redeemedOffers) {
					if (redeemedOffer.getOfferId() == mOffer.getId()) {
						Log.d(TAG, "Redemption code was found for offer: " + mOffer.getId());
						changeToRedeemedState(redeemedOffer.getRedemptionText());
						return;
					}
				}

				//Offer was not found among all redeemed offers
				Log.d(TAG, "Redeemed offer was not found for offer: " + mOffer.getId());
				UIUtils.showAlertDialog(getActivity(), getString(R.string.title_error), getString(R.string.offer_details_get_redeemed_offer_error));
			}

			@Override
			public void onFailure(VMobException error) {
				Log.e(TAG, "Failed to get redeemed offers", error);

				progressDialog.dismiss();

				UIUtils.showAlertDialog(
						getActivity(),
						getString(R.string.title_error),
						getString(R.string.offer_details_get_redeemed_offers_error, PlexureUtils.getErrorText(error)));
			}
		});
	}

	//This method is called when the offer was already redeemed and we received a redemption code.
	//It changes the visual appearance of the fragment: redeem button will be hidden,
	//redemption code will be visible.
	private void changeToRedeemedState(String redemptionCode) {
		//Hide redeem button, the offer was redeemed already
		mRedeemButton.setVisibility(View.GONE);

		//Show redemption code
		mRedemptionCodeContainer.setVisibility(View.VISIBLE);
		mRedemptionCode.setText(redemptionCode);
	}

}

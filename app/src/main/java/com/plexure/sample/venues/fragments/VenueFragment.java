package com.plexure.sample.venues.fragments;

import android.Manifest;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.plexure.sample.R;
import com.plexure.sample.main.constants.ActivityTracking;
import com.plexure.sample.utils.PlexureUtils;
import com.plexure.sample.venues.providers.VenueProvider;
import com.plexure.sample.venues.utils.LocationUtils;

import java.util.HashMap;
import java.util.List;

import co.vmob.sdk.VMob;
import co.vmob.sdk.VMobException;
import co.vmob.sdk.content.venue.model.Venue;

/**
 * Fragment for displaying venues on a Google Map view
 * <p/>
 * Fetching and handling the venue data the fragment is depending on the {@link VenueProvider} class.
 * Venues are downloaded and stored locally before added to the map.
 * If something went wrong with the data downloading then the error is shown on the screen instead
 * of the map view.
 *
 * @author Almos Rajnai (almos.rajnai@plexure.com)
 */
public class VenueFragment extends Fragment {

	private static final String TAG = VenueFragment.class.getName();

	private MapView mMapView;
	private GoogleMap mGoogleMap;
	private Venue mSelectedVenue;

	private ProgressBar mVenueProgressBar;
	private View mDetailsContainer;
	private TextView mVenueName;
	private TextView mVenueAddress;
	private TextView mErrorTextView;

	//Mapping list between map markers and venues
	private final HashMap<Marker, Venue> mMarkerList = new HashMap<>();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_venues, container, false);

		//Init UI view fields
		mMapView = (MapView) view.findViewById(R.id.map_view);
		mVenueProgressBar = (ProgressBar) view.findViewById(R.id.venue_progress_bar);
		mDetailsContainer = view.findViewById(R.id.details_container);
		mVenueName = (TextView) view.findViewById(R.id.details_name);
		mVenueAddress = (TextView) view.findViewById(R.id.details_address);
		mErrorTextView = (TextView) view.findViewById(R.id.error_text);

		//Set up directions button on venue details view
		Button directionsButton = (Button) view.findViewById(R.id.directions);
		directionsButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				LocationUtils.getDirectionsToVenue(getActivity(), mSelectedVenue);
			}
		});

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		//We need to initialize the map here or the app will crash when trying to zoom into the map
		MapsInitializer.initialize(getActivity().getApplicationContext());

		//We must call the lifecycle methods of the MapView class as specified here: https://developers.google.com/maps/documentation/android/map#mapview
		mMapView.onCreate(savedInstanceState);

		mMapView.getMapAsync(new OnMapReadyCallback() {

			@Override
			public void onMapReady(GoogleMap googleMap) {
				mGoogleMap = googleMap;

				//Set up map UI
				UiSettings mapSettings = googleMap.getUiSettings();
				mapSettings.setZoomControlsEnabled(true);
				mapSettings.setMyLocationButtonEnabled(true);
				mapSettings.setCompassEnabled(true);

				if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
					Log.d(TAG, "No location permission, so not showing current user location in the map");
					return;
				} else {
					googleMap.setMyLocationEnabled(true);
				}

				//Clicking on markers makes the app showing the venue data on the screen
				googleMap.setOnMarkerClickListener(markerClickListener);

				//Clicking on map hides detail pane
				googleMap.setOnMapClickListener(onMapClickListener);

				//Load data to screen if available
				List<Venue> venues = VenueProvider.getInstance().getVenueList();
				if (venues != null) {
					addVenueItems(venues);
				}
			}
		});
	}

	@Override
	public void onStart() {
		super.onStart();

		//Send view impression Plexure Activity for the screen.
		//We specify two parameters only: the unique code for the page and the generic
		//category for the page type.
		VMob.getInstance().getActivityTracker().logPageImpression(
				ActivityTracking.PAGE_IMPRESSION_VENUES,
				null, null, null,
				ActivityTracking.PAGE_IMPRESSION_CODE_CONTENT);
	}

	@Override
	public void onResume() {
		super.onResume();

		//We must call the callback methods of the MapView class as specified here: https://developers.google.com/maps/documentation/android/map#mapview
		mMapView.onResume();

		//Subscribe for venue loading message
		LocalBroadcastManager.getInstance(getActivity().getApplicationContext())
				.registerReceiver(venueMessageReceiver, new IntentFilter(VenueProvider.BROADCAST_VENUES_LOADING_FINISHED));
	}

	@Override
	public void onPause() {
		//Unregister from venue loading message
		LocalBroadcastManager.getInstance(getActivity().getApplicationContext())
				.unregisterReceiver(venueMessageReceiver);

		super.onPause();

		//We must call the callback methods of the MapView class as specified here: https://developers.google.com/maps/documentation/android/map#mapview
		mMapView.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		//Remove markers from map
		if (mGoogleMap != null) {
			mGoogleMap.clear();
		}

		//We must call the callback methods of the MapView class as specified here https://developers.google.com/maps/documentation/android/map#mapview
		if (mMapView != null) {
			mMapView.onDestroy();
		}
	}

	//Add all venues as markers to the map, also maintain the mapping between the markers and
	//the venues to be able to get data from the venue when the user clicks on the marker.
	private void addVenueItems(List<Venue> venueList) {
		if (mGoogleMap == null) {
			Log.e(TAG, "Google maps was not ready.");
			return;
		}

		mMarkerList.clear();
		mGoogleMap.clear();

		mVenueProgressBar.setVisibility(View.GONE);

		boolean isFirst = true;
		for (Venue venue : venueList) {
			Double latitude = venue.getLatitude();
			Double longitude = venue.getLongitude();

			//It is possible that the venue does not have coordinates, for example when it
			//is a web shop. We do need the coordinates for the map, so venues with
			//no coordinates will be skipped.
			if (latitude != null && longitude != null) {
				//Create new marker for the venue on the map
				Marker marker = mGoogleMap.addMarker(new MarkerOptions()
						.position(new LatLng(latitude, longitude))
						.flat(true));

				//Store the connection between the new marker and the venue in the mapping list
				mMarkerList.put(marker, venue);

				//Center the first on the map
				if (isFirst) {
					Location location = LocationUtils.getCurrentLocation(getActivity());
					if (location != null) {
						CameraPosition position = new CameraPosition.Builder()
								.target(new LatLng(location.getLatitude(), location.getLongitude()))
								.zoom(14)
								.build();
						mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
						isFirst = false;
					}
				}
			}
		}
	}

	private void showError(VMobException exception) {
		String errorText;

		mVenueProgressBar.setVisibility(View.GONE);

		//Do we have an exception object?
		if (exception != null) {
			//Format error text
			errorText = getString(R.string.venues_load_error, PlexureUtils.getErrorText(exception));
		} else {
			//We don't know what has happened, but the venues are not available
			errorText = getString(R.string.venues_unknown_load_error);
		}

		//Hide map view
		mMapView.setVisibility(View.GONE);

		//Show error text instead
		mErrorTextView.setText(errorText);
		mErrorTextView.setVisibility(View.VISIBLE);
	}

	private final GoogleMap.OnMarkerClickListener markerClickListener = new GoogleMap.OnMarkerClickListener() {

		@Override
		public boolean onMarkerClick(Marker marker) {

			//Try to find the venue by the clicked marker in the mapping list
			Venue venue = mMarkerList.get(marker);

			if (venue != null) {
				//Show details for the pin
				mVenueName.setText(venue.getName());

				//Save selected venue, will be used for getting directions later on
				mSelectedVenue = venue;

				//Center marker on the map
				mGoogleMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(venue.getLatitude(), venue.getLongitude())));

				String address = venue.getAddress();
				Location location = LocationUtils.getCurrentLocation(getActivity());

				//Do we have a current location?
				if (location != null) {
					//Yes, try to calculate the distance from the venue
					float distance = LocationUtils.getDistanceFromVenue(location, venue) / 1000.0f;

					//If the distance from current location is more than 300km then we show a text instead
					address += "\n" + (distance > 300.0f ? getString(R.string.venues_distance_is_too_big) : String.format(getString(R.string.venues_distance_text), distance));
				}

				mVenueAddress.setText(address);

				mDetailsContainer.setVisibility(View.VISIBLE);

				//Send Plexure Activity for the venue click.
				//We assume a generic click in this case, but specific information can be served as
				//context for the click via parameters which are not specified here.
				//The button click ID and the venue ID together gives enough information about the event.
				VMob.getInstance().getActivityTracker().logButtonClick(
						ActivityTracking.BUTTON_CLICK_VENUE,
						null,
						null,
						venue.getId(),
						ActivityTracking.BUTTON_CLICK_CODE_GENERIC);

				return true;
			}

			//This must not happen: we have not found the venue to the marker in the mapping list
			Log.e(TAG, "Could not find venue for clicked marker");

			return false;
		}
	};

	//Broadcast event receiver for handling venues loaded message from VenueProvider
	private final BroadcastReceiver venueMessageReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			//Venues are loaded, check result
			List<Venue> venueList = VenueProvider.getInstance().getVenueList();

			//Check if data is available, we assume that at least one venue must be there
			if (venueList != null && venueList.size() != 0) {
				//Data is ready, add it to the map
				addVenueItems(venueList);
			} else {
				//Failed to get venues, show error

				//Hide progress indicator
				mVenueProgressBar.setVisibility(View.GONE);

				//Show error pop-up (dialog will be postponed if fragment was not visible to the user)
				showError(VenueProvider.getInstance().getNetworkException());
			}
		}
	};

	private final GoogleMap.OnMapClickListener onMapClickListener = new GoogleMap.OnMapClickListener() {

		@Override
		public void onMapClick(LatLng latLng) {
			//Clicked outside of the markers: hide details view
			mDetailsContainer.setVisibility(View.GONE);
		}
	};
}

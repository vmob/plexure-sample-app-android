package com.plexure.sample.utils;

import android.content.Context;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Utility methods for file handling on the local storage
 * <p/>
 * Note: Please ignore this utility class, it does not contain any important information about
 * the implementation of the integration with the Plexure SDK.
 *
 * @author Almos Rajnai (almos.rajnai@plexure.com)
 */
public class FileUtils {

	public static void saveTextFile(Context context, String fileName, String text) throws IOException {
		FileWriter writer = new FileWriter(new File(context.getFilesDir(), fileName));
		writer.write(text);
		writer.close();
	}

	public static String loadTextFile(Context context, String fileName) throws IOException {
		FileReader reader = new FileReader(new File(context.getFilesDir(), fileName));

		BufferedReader br = new BufferedReader(reader);
		StringBuilder text = new StringBuilder();
		String line;

		while ((line = br.readLine()) != null) {
			text.append(line);
			text.append('\n');
		}
		br.close();

		return text.toString();
	}
}

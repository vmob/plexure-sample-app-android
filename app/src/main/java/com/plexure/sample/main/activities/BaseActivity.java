package com.plexure.sample.main.activities;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;

import com.plexure.sample.notification.GCMReceiver;
import com.plexure.sample.venues.providers.VenueProvider;

import co.vmob.sdk.VMob;

/**
 * Base {@link Activity} for all Activities with user interface in the sample app.
 *
 * @author Almos Rajnai (almos.rajnai@plexure.com)
 */
@SuppressLint("Registered")
public class BaseActivity extends Activity {

	private static final String TAG = BaseActivity.class.getName();

	@Override
	public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
		super.onCreate(savedInstanceState, persistentState);
	}

	@Override
	protected void onResume() {
		super.onResume();

		ActionBar actionBar = getActionBar();
		if (actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setHomeButtonEnabled(true);
		}

		//Send push message clicked Plexure Activity in case the startup intent has the data
		sendPushClickedActivity();

		//Check for necessary update for the locally cached venue data.
		//It is not necessary to check for update at every time when the main activity shows up on
		//the screen, this is just a simplified solution. It is up to the implementation project
		//to maintain the downloaded venue data.
		VenueProvider.getInstance().checkLocalData();

		//Let the Plexure SDK know about that the app was launched or brought forward,
		//so the SDK can send the app startup Plexure Activity if it was necessary.
		//The Activity is not sent every time when the following method is called,
		//the SDK decides about how often is it necessary to notify the platform
		//about the app launch.
		//The call must be added to all Android Activities which have a user interface.
		//In this sample app the base Android Activity used for this purpose, all other
		//Activities with user interface are inherited from this class.
		VMob.getInstance().getActivityTracker().logAppStartup();
	}

	//Detect if this Activity was launched from a push message notification and
	//handle sending the Plexure Activity for it.
	private void sendPushClickedActivity() {
		//Data from push message is served through the launch intent
		Intent intent = getIntent();

		//Detect if this Activity was launched from a push message notification click.
		//For more see message processing service implementation: GCMReceiver
		if (intent != null && intent.getBooleanExtra(GCMReceiver.EXTRA_NOTIFICATION_WAS_CLICKED, false)) {
			//Yes, we have found the flag from processing service
			int messageId = intent.getIntExtra(GCMReceiver.EXTRA_NOTIFICATION_ID, -1);

			Log.d(TAG, "Push message notification was clicked by user, sending Plexure Activity with message ID: " + messageId);

			// Log push message click activity with the Plexure backend
			VMob.getInstance().getActivityTracker().logPushMessageClick(messageId);

			//Remove the push notification-related extras from the intent to avoid triggering
			//the Plexure Activity again if the intent was reused for relaunching the main Activity.
			getIntent().removeExtra(GCMReceiver.EXTRA_NOTIFICATION_WAS_CLICKED);
			getIntent().removeExtra(GCMReceiver.EXTRA_NOTIFICATION_ID);
		}
	}
}

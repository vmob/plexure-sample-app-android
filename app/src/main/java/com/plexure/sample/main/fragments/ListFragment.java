package com.plexure.sample.main.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.plexure.sample.R;
import com.plexure.sample.main.constants.ActivityTracking;

import co.vmob.sdk.VMob;

/**
 * Abstract {@link Fragment} class to be extended by {@link Fragment}s that implement a list of
 * items to show on the screen.
 *
 * @author Franco Sabadini (franco.sabadini@plexure.com)
 */
public abstract class ListFragment<T> extends Fragment {

	@StringRes
	protected int mLodingTextRes;

	protected String mPageImpressionName;

	private TextView mHeaderText;
	private ListView mListView;
	protected ArrayAdapter<T> mAdapter;


	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_list, container, false);

		mHeaderText = (TextView) view.findViewById(R.id.list_header);
		mHeaderText.setText(mLodingTextRes);
		mListView = (ListView) view.findViewById(R.id.list);
		mListView.setEmptyView(mHeaderText);

		return view;
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		Activity activity = getActivity();

		mAdapter = createListAdapter(activity);
		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, final int position, long l) {
				showItemDetailsScreen(mAdapter.getItem(position));
			}

		});

		//When the view is created then we try to fetch the advertisements
		loadItems();
	}

	protected abstract ArrayAdapter<T> createListAdapter(Activity activity);

	protected abstract void showItemDetailsScreen(T item);

	protected abstract void loadItems();

	protected void setEmptyListHeader(@StringRes int textId, Object... params) {
		//Set the header only if the fragment is added, otherwise the app would crash
		//on resolving the string resource
		if (isResumed()) {
			mHeaderText.setText(getString(textId, params));
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		//Send view impression Plexure Activity for the screen.
		//We specify two parameters only: the unique code for the page and the generic
		//category for the page type.
		VMob.getInstance().getActivityTracker().logPageImpression(
				mPageImpressionName,
				null, null, null,
				ActivityTracking.PAGE_IMPRESSION_CODE_CONTENT);

		//Simple workaround for sending items impression Plexure Activities when this fragment
		//was brought back to foreground. The impressions are sent from the getView() method of
		//the adapter, which will not be called again when the fragment is restored without
		//invalidating the views on the screen.
		mListView.invalidateViews();
	}

}

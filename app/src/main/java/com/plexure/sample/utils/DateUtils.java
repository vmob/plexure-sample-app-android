package com.plexure.sample.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * Utility methods for date-related operations
 *
 * @author Almos Rajnai (almos.rajnai@plexure.com)
 */
public class DateUtils {

	/**
	 * Refresh period in seconds for venues, currently set to 7 days
	 */
	public static final long DATA_REFRESH_PERIOD = 7 * 24 * 60 * 60;


	/**
	 * Get current time in UTC time zone.
	 *
	 * @return Current UTC time elapsed since Epoch in milliseconds.
	 */
	public static long getCurrentUtcTimeInMilliseconds() {
		return getNewUtcCalendar().getTimeInMillis();
	}

	/**
	 * Is data update is needed: provided timestamp is compared to the global
	 * data refresh policy. See constant: {@link #DATA_REFRESH_PERIOD}
	 *
	 * @param timestamp Last time the data was updated (UTC).
	 * @return True if data is expired and update needed, false otherwise.
	 */
	public static boolean isDataUpdateNeeded(Date timestamp) {
		//If there was no time stamp then we need an update
		if (timestamp == null) {
			return true;
		}

		Date now = new Date(DateUtils.getCurrentUtcTimeInMilliseconds());
		Date lastAllowedRefreshDate = new Date(timestamp.getTime() + DATA_REFRESH_PERIOD * 1000);

		//If the date is after the specified period then return true
		return now.after(lastAllowedRefreshDate);
	}

	private static Calendar getNewUtcCalendar() {
		return new GregorianCalendar(TimeZone.getTimeZone("UTC"));
	}

	/**
	 * All {@link Date} objects returned by the Plexure SDK have not taken time zone into account, which is a bug, so we have
	 * to add the time zone offset to it.
	 *
	 * @param date {@link Date} with correct time zone set up.
	 */
	public static Date addTimeZoneOffset(Date date) {
		return new Date(date.getTime() + TimeZone.getDefault().getRawOffset() + Calendar.getInstance().get(Calendar.DST_OFFSET));
	}

}

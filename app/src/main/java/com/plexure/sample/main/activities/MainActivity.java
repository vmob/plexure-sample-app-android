package com.plexure.sample.main.activities;

import android.Manifest;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.widget.Toast;

import com.plexure.sample.R;
import com.plexure.sample.main.constants.NavigationListItem;
import com.plexure.sample.main.fragments.NavigationDrawerFragment;

import java.util.LinkedList;

/**
 * Standard navigation drawer-handling activity, derived from the Android SDK example code.
 * <p/>
 * Note: Please ignore this Activity class, it does not contain any important information about
 * the implementation of the integration with the Plexure SDK.
 * For Plexure SDK-related implementation details review the {@link BaseActivity} class.
 */
public class MainActivity extends BaseActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks {

	private static final int PERMISSION_REQUEST_LOCATION = 1;

	private NavigationDrawerFragment mNavigationDrawerFragment;
	private LinkedList<Integer> mActionBarTitleStack = new LinkedList<>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mNavigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.navigation_drawer);
		mNavigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));

		requestPermissionIfNecessary();
	}

	/**
	 * We need to request permissions that are considered dangerous at run time if the app is targeting SDK 23
	 * (Marshmallow) and above. {@link Manifest.permission#ACCESS_FINE_LOCATION} and {@link Manifest.permission#ACCESS_COARSE_LOCATION}
	 * are needed for getting the location of the device. However, since these two permissions are in the same group we
	 * only need to request one, then the other one will be granted automatically.
	 *
	 * @see <a href="http://developer.android.com/training/permissions/requesting.html">Requesting Permissions at Run Time</a>
	 * @see <a href="http://developer.android.com/guide/topics/security/permissions.html#perm-groups">Permission groups</a>
	 */
	private void requestPermissionIfNecessary() {
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			// If the app has requested this permission previously and the user denied the request, we need to show an
			// explanation to the user.
			if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
				// Show an explanation to the user *asynchronously* -- don't block
				// this thread waiting for the user's response! After the user
				// sees the explanation, try again to request the permission.
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle(R.string.permission_request_dialog_title);
				builder.setMessage(R.string.location_permission_explanation);
				builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// Request the permission again
						ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
								PERMISSION_REQUEST_LOCATION);
					}
				});
				builder.create().show();
			} else {
				// No explanation needed, we can request the permission.
				ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
						PERMISSION_REQUEST_LOCATION);
			}
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
		switch (requestCode) {
			case PERMISSION_REQUEST_LOCATION: {
				// If request is cancelled, the result arrays are empty.
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					// TODO The permissions were granted, execute any functionality depending on these permissions here.
				} else {
					// TODO Disable functionality that needs fine location permission from the app.
					Toast.makeText(this, R.string.location_permission_denied_toast, Toast.LENGTH_SHORT).show();
				}
			}
		}
	}

	@Override
	public void onNavigationDrawerItemSelected(int position) {
		NavigationListItem fragment = NavigationListItem.getItem(position);
		openFragment(fragment.getNameId(), fragment.newFragmentInstance());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!mNavigationDrawerFragment.isDrawerOpen()) {
			restoreActionBar();
			return true;
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public void onBackPressed() {
		//Update screen title and disable sub-page mode for the navigation drawer, in case a sub-page screen was opened before.
		mActionBarTitleStack.pop();
		restoreActionBar();

		if (getFragmentManager().getBackStackEntryCount() <= 1) {
			mNavigationDrawerFragment.setSubPageModeEnabled(false);
		}

		super.onBackPressed();
	}

	/**
	 * Opens a top level fragment.
	 */
	public void openFragment(@StringRes int screenTitleRes, Fragment fragment) {
		getFragmentManager().beginTransaction()
				.replace(R.id.container, fragment)
				.commit();

		mActionBarTitleStack.clear();
		mActionBarTitleStack.push(screenTitleRes);
		restoreActionBar();
	}

	/**
	 * Opens a child fragment which will be added to backstack so the user can navigate back to the parent fragment
	 * when pressing back button. e.g. the parent fragment is showing all offers, then the child fragment is the
	 * details of a specific offer.
	 */
	public void openChildFragment(@StringRes int screenTitleRes, Fragment fragment) {
		mActionBarTitleStack.push(screenTitleRes);
		restoreActionBar();

		//Replace current fragment on the screen with the provided fragment and add step to navigation back stack
		getFragmentManager().beginTransaction()
				.replace(R.id.container, fragment)
				.addToBackStack(fragment.getClass().getName())
				.commit();

		//Change behaviour of navigation drawer to sub-page mode
		mNavigationDrawerFragment.setSubPageModeEnabled(true);
	}

	private void restoreActionBar() {
		ActionBar actionBar = getActionBar();
		if (actionBar != null && mActionBarTitleStack.peek() != null) {
			actionBar.setTitle(mActionBarTitleStack.peek());
		}
	}

}

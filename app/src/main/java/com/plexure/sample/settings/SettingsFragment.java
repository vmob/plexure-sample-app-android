package com.plexure.sample.settings;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.plexure.sample.R;
import com.plexure.sample.main.constants.ActivityTracking;

import co.vmob.sdk.VMob;
import co.vmob.sdk.configuration.IConfigurationManager;

/**
 *  Fragment for turning on/off activities tracking, geofence monitoring, beacon scanning features.
 *
 * @author Ludy Su (ludy.su@plexure.com)
 */
public class SettingsFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_settings, container, false);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		CompoundButton activitiesSwitch = (CompoundButton) getView().findViewById(R.id.swtich_activities);
		CompoundButton geofencesSwitch = (CompoundButton) getView().findViewById(R.id.swtich_geofence);
		CompoundButton beaconsSwitch = (CompoundButton) getView().findViewById(R.id.swtich_beacon);

		final IConfigurationManager configurationManager = VMob.getInstance().getConfigurationManager();

		activitiesSwitch.setChecked(configurationManager.isActivitiesTrackingEnabled());
		geofencesSwitch.setChecked(configurationManager.isGeofencesMonitoringEnabled());
		beaconsSwitch.setChecked(configurationManager.isBeaconsMonitoringEnabled());

		activitiesSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				configurationManager.enableActivitiesTracking(isChecked);
			}
		});

		geofencesSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				configurationManager.enableGeofencesMonitoring(isChecked);
			}
		});

		beaconsSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				configurationManager.enableBeaconsMonitoring(isChecked);
			}
		});
	}

	@Override
	public void onStart() {
		super.onStart();

		//Send view impression Plexure Activity for the screen.
		//We specify two parameters only: the unique code for the page and the generic
		//category for the page type.
		VMob.getInstance().getActivityTracker().logPageImpression(
				ActivityTracking.PAGE_IMPRESSION_SETTINGS,
				null, null, null,
				ActivityTracking.PAGE_IMPRESSION_CODE_CONTENT);
	}

}

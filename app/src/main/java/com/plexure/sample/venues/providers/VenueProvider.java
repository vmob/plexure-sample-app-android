package com.plexure.sample.venues.providers;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.plexure.sample.utils.DateUtils;
import com.plexure.sample.utils.FileUtils;
import com.plexure.sample.utils.PreferencesUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import co.vmob.sdk.VMob;
import co.vmob.sdk.VMobException;
import co.vmob.sdk.content.venue.model.Venue;
import co.vmob.sdk.content.venue.model.VenueSearchCriteria;

/**
 * Venue data provider class
 * Implements downloading of venue data in chunks, storing and updating venues.
 * <p/>
 * The venue data is downloaded in the background, a notification broadcast:
 * ({@link VenueProvider#BROADCAST_VENUES_LOADING_FINISHED} will be sent to the subscribers
 * (venue fragment) when downloading is finished or has failed.
 * If the venues were downloaded previously then will be loaded into memory from the local storage.
 * <p/>
 * A timestamp is also saved together with the venues to the local storage. Venue data will be
 * updated after a certain period of time which is defined as a constant:
 * {@link DateUtils#DATA_REFRESH_PERIOD}
 *
 * @author Almos Rajnai (almos.rajnai@plexure.com)
 */
public class VenueProvider {

	private static final String TAG = VenueProvider.class.getName();

	private static final String VENUE_CACHE_FILE_NAME = "venues.json";

	//Event name for broadcast intent when venue loading finished with a result
	public static final String BROADCAST_VENUES_LOADING_FINISHED = "venues_loading_finished";

	//Number of venues we are trying to download in request
	private static final int DOWNLOAD_CHUNK_SIZE = 200;

	//Number of retries for failed requests
	private static final int DOWNLOAD_RETRIES = 3;

	//Singleton instance of the class
	private static VenueProvider sInstance;

	//Semaphore for data check
	private final Object mDataCheckLock = new Object();

	//Statically stored venue and merchant list, loaded from local storage
	private static List<Venue> sVenueList;

	private final Context mContext;
	private final Gson mGson = new GsonBuilder().create();
	private final ArrayList<Venue> mDownloadedItems = new ArrayList<>();

	//Locally cached exception descriptor for any network communication failure
	private VMobException mPreviousNetworkException;

	//Flag for signaling if venue downloading is in progress to avoid starting the process multiple times
	private boolean mDownloadingInProgress;

	//Number of remaining retries on download failure
	private int mRetries;

	private AsyncTask<String, Void, String> mVenueLoaderTask;

	private VenueProvider(Context context) {
		mContext = context;
	}

	public static VenueProvider getInstance() {
		if (sInstance == null) {
			throw new RuntimeException("VenueProvider is not initialized");
		}

		return sInstance;
	}

	/**
	 * Initialize singleton instance of the class.
	 *
	 * @param context Application context
	 */
	public static void init(Context context) {
		sInstance = new VenueProvider(context);
	}

	/**
	 * Check local data for venues and start downloading/loading into memory according to whether
	 * the data is available and not expired yet.
	 */
	public void checkLocalData() {
		synchronized (mDataCheckLock) {
			Date timestamp = PreferencesUtils.getVenueUpdateTimestamp();
			if (DateUtils.isDataUpdateNeeded(timestamp)) {
				//Venues update is needed
				startDataDownloading();
			}

			if (timestamp != null) {
				//If we have data already then start loading it,
				//otherwise it will be started only when download is finished
				startDataLoading();
			}
		}
	}

	/**
	 * Get venue list from memory
	 * <p/>
	 * Note: if returned list is null then check {@link #getNetworkException()} for a possible
	 * network issue which blocked the data loading.
	 *
	 * @return Venue list or null if not available at the moment
	 */
	public List<Venue> getVenueList() {
		return sVenueList;
	}

	private void startDataDownloading() {
		if (!mDownloadingInProgress) {
			Log.d(TAG, "Start downloading venue data in chunks");

			mDownloadingInProgress = true;

			//Reset download state
			mRetries = DOWNLOAD_RETRIES;
			mDownloadedItems.clear();

			//Start downloading from beginning
			asyncDownloadDataChunkFromOffset(0);
		}
	}

	private void asyncDownloadDataChunkFromOffset(final int offset) {
		Log.d(TAG, "Download venue " + DOWNLOAD_CHUNK_SIZE + " venues from offset " + offset);

		//Download venues in chunks, chunk size is predefined as a constant,
		//offset and limit parameters are maintaining the sliding window for the download process.
		VenueSearchCriteria criteria = new VenueSearchCriteria.Builder()
				.setOffset(offset)
				.setLimit(DOWNLOAD_CHUNK_SIZE).create();

		VMob.getInstance().getVenuesManager().getVenues(
				criteria, new VMob.ResultCallback<List<Venue>>() {
					@Override
					public void onSuccess(List<Venue> downloadedVenues) {
						Log.d(TAG, "Downloading venues chunk was successful");

						//Reset retries
						mRetries = DOWNLOAD_RETRIES;

						//Reset previously cached error
						mPreviousNetworkException = null;

						//Add items to the list in memory
						mDownloadedItems.addAll(downloadedVenues);

						//Have we received all venues we were asking for?
						int downloadedItems = downloadedVenues.size();
						if (downloadedItems == DOWNLOAD_CHUNK_SIZE) {
							//There are more to download: download next chunk
							asyncDownloadDataChunkFromOffset(offset + DOWNLOAD_CHUNK_SIZE);
						} else {
							Log.d(TAG, "Downloading of venues is finished, save data to local store");

							//In the context of a background task we save all venues to the local
							//storage. For sake of simplicity venues are saved as a JSON structure,
							//but it is advised to save only the required data from the venue
							//objects into a local database to save storage space and speed up
							//manipulation of data.
							new AsyncTask<String, Void, String>() {

								@Override
								protected String doInBackground(String... params) {
									try {
										//Save venues to JSON to local storage
										FileUtils.saveTextFile(mContext, VENUE_CACHE_FILE_NAME, mGson.toJson(mDownloadedItems));

										//Update timestamp
										PreferencesUtils.saveNewVenueUpdateTimestamp();

										Log.d(TAG, "Saving venues to local file was successful");
									} catch (Exception e) {
										Log.e(TAG, "Saving venues to local file has failed", e);
									}

									//Clear downloaded data from memory
									mDownloadedItems.clear();

									//Downloading is finished
									mDownloadingInProgress = false;

									//Load it back from local file
									startDataLoading();

									return null;
								}
							}.execute("");
						}
					}

					@Override
					public void onFailure(VMobException error) {
						Log.e(TAG, "Error while updating venues", error);

						//Do we have retries left?
						if (mRetries != 0) {
							Log.d(TAG, "Retry venue downloading, retry count: " + mRetries);
							//Try again from the same offset
							mRetries--;
							asyncDownloadDataChunkFromOffset(offset);
						} else {
							Log.d(TAG, "Retries for venue downloading wore off, downloading has failed");

							//No more retries, save error for processing it later on
							mPreviousNetworkException = error;

							//Downloading has failed, let's try again next time
							mDownloadingInProgress = false;

							//Try to load available data
							startDataLoading();
						}
					}
				});
	}

	//Returns saved network exception from failed downloading process
	public VMobException getNetworkException() {
		return mPreviousNetworkException;
	}

	private void startDataLoading() {
		//If we don't have a venue loader task yet then let's create it now
		if (mVenueLoaderTask == null) {
			mVenueLoaderTask = new VenueLoaderTask();
		}

		//Start loading data into memory, if it was not started yet
		if (mVenueLoaderTask.getStatus() == AsyncTask.Status.PENDING) {
			Log.d(TAG, "Start loading venue data into memory");
			mVenueLoaderTask.execute();
		}
	}

	/**
	 * Venue data loader class, reads all venue information from local storage in an async task
	 * then triggers loading the data to the map.
	 */
	private class VenueLoaderTask extends AsyncTask<String, Void, String> {

		private Venue[] mVenueList;

		@Override
		protected String doInBackground(String... params) {
			//Try to load venues from local file
			try {
				String venueList = FileUtils.loadTextFile(mContext, VENUE_CACHE_FILE_NAME);
				mVenueList = mGson.fromJson(venueList, Venue[].class);
			} catch (Exception e) {
				//Something went wrong with the loading, we don't have the list
				Log.e(TAG, "Failed to load venues from local storage", e);
				mVenueList = null;
			}

			return null;
		}

		protected void onPostExecute(String result) {
			//Copy list to static field
			if (mVenueList != null) {
				sVenueList = Arrays.asList(mVenueList);
			} else {
				sVenueList = null;
			}

			//Notify subscribers
			notifySubscribers();

			cleanup();

			if (sVenueList != null) {
				Log.d(TAG, "Venues are loaded from local storage");
			} else {
				Log.w(TAG, "There are no locally stored venues to load");
			}
		}

		@Override
		protected void onCancelled(String s) {
			cleanup();
		}

		private void cleanup() {
			mVenueList = null;
			mVenueLoaderTask = null;
		}
	}

	//Notify any subscribers about finished loading of venues into memory
	private void notifySubscribers() {
		Intent intent = new Intent(BROADCAST_VENUES_LOADING_FINISHED);
		LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
	}
}

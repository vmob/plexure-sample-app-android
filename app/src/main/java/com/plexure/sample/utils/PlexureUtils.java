package com.plexure.sample.utils;

import co.vmob.sdk.VMobException;
import co.vmob.sdk.content.offer.model.Offer;
import co.vmob.sdk.network.error.ServerApiException;

/**
 * Utility methods for Plexure SDK-related operations
 *
 * @author Almos Rajnai (almos.rajnai@plexure.com)
 */
public class PlexureUtils {

	/**
	 * Various states of an offer
	 */
	public enum OfferState {
		/**
		 * Offer is available for redemption
		 */
		AVAILABLE,

		/**
		 * Offer is not respawnable and not available currently, but it will be later on.
		 * The offer was not redeemed before.
		 */
		NOT_AVAILABLE_NON_RESPAWNABLE,

		/**
		 * Offer is respawnable and not available currently, but will be available today.
		 */
		NOT_AVAILABLE_RESPAWNABLE,

		/**
		 * Offer is not respawnable and it was redeemed previously: not available any longer.
		 */
		ALREADY_REDEEMED_NOT_RESPAWNABLE,

		/**
		 * Offer is respawnable, but it was redeemed previously and a couple more days to go
		 * before it will be active again.
		 */
		ALREADY_REDEEMED_RESPAWNABLE
	}

	public static String getErrorText(VMobException exception) {
		//Usually the returned exception is a ServerApiException instance, in this case we can get
		//more information about the actual failure.
		if (exception instanceof ServerApiException) {
			return String.format("ServerApiException: %s", ((ServerApiException) exception).getError().name());
		} else {
			// This is not a ServerApiException, handle it as a generic error
			return String.format("%s: %s", exception.getClass().getSimpleName(), exception.getMessage());
		}
	}

	/**
	 * Evaluate current state of an offer according to various fields
	 * See <a http://support.plexure.com/hc/en-us/sections/201867063-Working-with-Offers>Working with Offers</>.
	 *
	 * @param offer
	 * 		Offer to evaluate
	 *
	 * @return Current state of the offer
	 */
	public static OfferState evaluateVoucherState(Offer offer) {
		//Is the offer available right now?
		if (offer.isActive()) {
			//Yes, it can be redeemed
			return OfferState.AVAILABLE;
		} else {

			//Is this a respawning offer?
			if (offer.isRespawning()) {
				//Yes, are there any more days left before respawn?
				if (offer.getRespawnDaysNumber() == 0) {
					//No, then it will be available today, just not yet
					return OfferState.NOT_AVAILABLE_RESPAWNABLE;
				} else {
					//Yes, it was redeemed already and still couple days to go until it will be available again
					return OfferState.ALREADY_REDEEMED_RESPAWNABLE;
				}
			}

			//This is not a respawning offer
			//Was it redeemed previously?
			if (offer.getRedemptionCount() == 0) {
				//No, it will be available, just not yet
				return OfferState.NOT_AVAILABLE_NON_RESPAWNABLE;
			}

			//Offer was redeemed previously and it is not respawnable: not available any more
			return OfferState.ALREADY_REDEEMED_NOT_RESPAWNABLE;
		}
	}
}

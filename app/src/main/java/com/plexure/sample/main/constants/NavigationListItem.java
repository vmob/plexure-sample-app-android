package com.plexure.sample.main.constants;

import android.app.Fragment;
import android.content.Context;
import android.support.annotation.StringRes;

import com.plexure.sample.BuildConfig;
import com.plexure.sample.R;
import com.plexure.sample.advertisements.fragments.AdvertisementsFragment;
import com.plexure.sample.loyaltycard.fragments.StampCardsFragment;
import com.plexure.sample.offers.fragments.OffersFragment;
import com.plexure.sample.profile.fragments.ProfileFragment;
import com.plexure.sample.settings.SettingsFragment;
import com.plexure.sample.venues.fragments.VenueFragment;

/**
 * List of menu items in the navigation drawer
 * <p/>
 * Note: Please ignore this enum, it does not contain any important information about
 * the implementation of the integration with the Plexure SDK.
 *
 * @author Almos Rajnai (almos.rajnai@plexure.com)
 */
public enum NavigationListItem {
	ADVERTISEMENTS(R.string.ads_screen_title, AdvertisementsFragment.class),
	OFFERS(R.string.offers_screen_title, OffersFragment.class),
	VENUES(R.string.venues_screen_title, VenueFragment.class),
	STAMP_CARDS(R.string.stamp_card_screen_title, StampCardsFragment.class),
	PROFILE(R.string.profile_screen_title, ProfileFragment.class),
	SETTING(R.string.settings_screen_title, SettingsFragment.class),
	// Keeps diagnostics as the LAST item. It's used to launch an activity, so no associated fragment
	DIAGNOSTICS(R.string.diagnostics_screen_title, null);

	private final int mNameId;
	private final Class<Fragment> mFragment;

	<T extends Fragment> NavigationListItem(@StringRes int nameId, Class<T> fragmentClass) {
		mNameId = nameId;

		@SuppressWarnings("unchecked")
		Class<Fragment> fragment = (Class<Fragment>) fragmentClass;
		mFragment = fragment;
	}

	public static String[] getNames(Context context) {
		NavigationListItem[] items = values();

		int size = items.length;
		if (!BuildConfig.DEBUG) {
			// Only shows diagnostics view when build variant is DEBUG, while on a normal build it will become hidden.
			size--;
		}
		String[] names = new String[size];
		for (int i = 0; i < size; i++) {
			names[i] = context.getString(items[i].getNameId());
		}

		return names;
	}

	public static NavigationListItem getItem(int index) {
		return values()[index];
	}

	public int getNameId() {
		return mNameId;
	}

	public Fragment newFragmentInstance() {
		try {
			Fragment fragment = (Fragment) Class.forName(mFragment.getName()).newInstance();
			return fragment;
		} catch (Exception e) {
			//This is not going to happen unless the app runs out of memory or something is wrong
			//with the layout inflation, but just in case we catch all exceptions
			throw new RuntimeException("Unable to create fragment from class: " + mFragment.getName(), e);
		}
	}

}
